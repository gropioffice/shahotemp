﻿using GropCommonLib;
using DensanKyuuyo.Common;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System;

namespace DensanKyuuyo.Bl
{
    internal class LoginBl
    {
        /// <summary>
        /// ログインチェック
        /// </summary>
        /// <param name="employeeNumber">社員番号</param>
        /// <param name="password">パスワード</param>
        /// <returns>ログイン結果</returns>
        public bool GetEmployeeInfos(string employeeNumber, string password)
        {
            PublicEmployee parm = new PublicEmployee();
            parm.employee_no = employeeNumber;
            parm.password_digest = GPasswordUtil.Encrypt(password);
            parm.system_name = Constants.SystemName;

            var list = GropShareSqlMapper.GetInstance().QueryForList<PublicEmployee>("SelectEmployeeLoginInfo", parm);

            if (1 != list.Count)
            {
                return false;
            }

            PublicEmployee emp = list[0];

            Program.loginInfo.employee_no = emp.employee_no;
            Program.loginInfo.name = emp.name;
            Program.loginInfo.name_kana = emp.name_kana;
            Program.loginInfo.company_code = emp.company_code;
            Program.loginInfo.department_code = emp.department_code;
            Program.loginInfo.position_code = emp.position_code;
            Program.loginInfo.password_digest = emp.password_digest;
            Program.loginInfo.mobile_phone = emp.mobile_phone;
            Program.loginInfo.mail_address = emp.mail_address;
            Program.loginInfo.mobile_mail_address = emp.mobile_mail_address;
            Program.loginInfo.remarks = emp.remarks;
            Program.loginInfo.retirement_flag = emp.retirement_flag;
            Program.loginInfo.regist_date = emp.regist_datetime;
            Program.loginInfo.regist_person_id = emp.regist_id;
            Program.loginInfo.update_date = emp.update_datetime;
            Program.loginInfo.update_person_id = emp.update_id;
            Program.loginInfo.computer_name = Environment.MachineName;
            Program.loginInfo.computer_user_name = Environment.UserName;

            return true;
        }
    }
}