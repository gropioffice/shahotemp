﻿using GropCommonLib;
using IBatisNet.Common;
using DensanKyuuyo.Common;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using DensanKyuuyo.View;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DensanKyuuyo.Bl
{
    public class BasicInfoBl
    {
        /// <summary>
        /// 基本情報を取得する。
        /// </summary>
        /// <param name="dspchw_code">派遣社員コード</param>
        /// <returns>基本情報リスト</returns>
        public BasicInfoModel GetBasic_Info(string dspchw_code)
        {
            var param = new BasicInfoModel();
            param.dispatch_worker_code = dspchw_code;

            return GropShareSqlMapper.GetInstance().QueryForObject<BasicInfoModel>("SelectBasicInfo", param);
        }

        /// <summary>
        /// 基本情報を登録する。
        /// </summary>
        /// <param name="ins_data">登録リスト</param>
        /// <param name="dt">扶養データテーブル</param>
        /// <returns>登録結果</returns>
        public bool Basic_InfoInsert(BasicInfoModel ins_data, DataTable dt)
        {
            using (IDalSession session = GropShareSqlMapper.GetInstance().BeginTransaction())
            {
                var bi = new BasicInfoModel();
                bi = GropShareSqlMapper.GetInstance().QueryForObject<BasicInfoModel>("SelectBasicInfo", ins_data);
                if (bi != null)
                {
                    session.Complete();
                    return false;                   // 他で登録された
                }

                GropShareSqlMapper.GetInstance().Insert("InsertBasicInfo", ins_data);

                Fuyou_Info_InsertUpdate(dt);

                session.Complete();
                return true;
            }
        }

        /// <summary>
        /// 基本情報を更新する。
        /// </summary>
        /// <param name="upd_data">更新リスト</param>
        /// <param name="lastupdateat">最終更新日時</param>
        /// <param name="dt">扶養データテーブル</param>
        /// <returns>更新結果</returns>
        public bool Basic_InfoUpdate(BasicInfoModel upd_data, DateTime? lastupdateat, DataTable dt)
        {
            using (IDalSession session = GropShareSqlMapper.GetInstance().BeginTransaction())
            {
                var bi = new BasicInfoModel();
                var param = new BasicInfoModel();
                param.dispatch_worker_code = upd_data.dispatch_worker_code;

                bi = GropShareSqlMapper.GetInstance().QueryForObject<BasicInfoModel>("SelectBasicInfo", param);
                if (bi == null)
                {
                    session.Complete();
                    return false;                   // レコードが削除された
                }

                if (bi.updated_at != lastupdateat)
                {
                    session.Complete();
                    return false;                   // 他で更新された
                }

                GropShareSqlMapper.GetInstance().Update("UpdateBasicInfo", upd_data);

                Fuyou_Info_InsertUpdate(dt);

                session.Complete();
                return true;
            }
        }

        /// <summary>
        /// 扶養情報を取得する。
        /// </summary>
        /// <param name="syain_code">社員コード</param>
        /// <param name="hi_fuyousya_name">被扶養者氏名</param>
        /// <param name="tsuzukigara">続柄</param>
        /// <param name="birth_day">生年月日</param>
        /// <returns>扶養情報（一件）</returns>
        public FuyouInfoModel Get_Fuyou_Info(string syain_code, string hi_fuyousya_name, int tsuzukigara, DateTime birth_day)
        {
            var param = new FuyouInfoModel();
            param.dispatch_worker_code = syain_code;
            param.hi_fuyousya_name = hi_fuyousya_name;
            param.tsuzukigara = tsuzukigara;
            param.birth_day = birth_day;

            return GropShareSqlMapper.GetInstance().QueryForObject<FuyouInfoModel>("SelectFuyouInfo", param);
        }

        /// <summary>
        /// 扶養情報を更新する。
        /// </summary>
        /// <param name="dt">扶養情報データテーブル</param>
        public void Fuyou_Info_InsertUpdate(DataTable dt)
        {
            var dic_続柄 = new Dictionary<int, string>();
            dic_続柄.Add(1, "夫");
            dic_続柄.Add(2, "妻");
            dic_続柄.Add(3, "子");
            dic_続柄.Add(4, "父母");
            dic_続柄.Add(5, "孫");
            dic_続柄.Add(6, "祖父母");
            dic_続柄.Add(7, "兄弟姉妹");
            dic_続柄.Add(8, "曽祖父母");
            dic_続柄.Add(9, "その他");

            foreach (DataRow dr in dt.Rows)
            {
                var fuyou = new FuyouInfoModel();

                fuyou.dispatch_worker_code = dr.Field<string>("社員コード");
                fuyou.hi_fuyousya_name = dr.Field<string>("被扶養者氏名");

                fuyou.tsuzukigara = dic_続柄.First(x => x.Value == dr.Field<string>("続柄")).Key;

                fuyou.birth_day = DateTime.Parse(dr.Field<string>("生年月日"));
                fuyou.fuyou_check_off = dr.Field<Boolean>("扶養から外れたらチェック外す") ? 1 : 0;
                fuyou.fuyou_remark = dr.Field<string>("備考") ?? string.Empty;
                fuyou.created_at = DateTime.Now;
                fuyou.created_by = Program.loginInfo.employee_no;
                fuyou.updated_at = fuyou.created_at;
                fuyou.updated_by = fuyou.created_by;

                if (GropShareSqlMapper.GetInstance().QueryForObject<FuyouInfoModel>("SelectFuyouInfo", fuyou) == null)
                {
                    GropShareSqlMapper.GetInstance().Insert("InsertFuyouInfo", fuyou);
                }
                else
                {
                    GropShareSqlMapper.GetInstance().Update("UpdateFuyouInfo", fuyou);
                }
            }
        }

        /// <summary>
        /// 基本情報の仮退職日のみを取得する。(EXCEL作成用)
        /// </summary>
        /// <param name="dspchw_codes">派遣社員コードリスト</param>
        /// <returns>基本情報リスト</returns>
        public List<BasicInfoModel> GetBasic_Info_KariTaisyokubi_Block(List<string> dspchw_codes)
        {
            var result = new List<BasicInfoModel>();

            foreach (var codes in dspchw_codes.Chunk(Constants.PostgresInClauseMaxCount))
            {
                var conditions = new Hashtable();
                conditions.Add("employee_codes", codes.ToArray());

                var data_karitaisyokubis = GropShareSqlMapper.GetInstance().QueryForList<BasicInfoModel>("SelectBasicInfoKariTaisyokubiBlock", conditions).ToList();
                result.AddRange(data_karitaisyokubis);
            }

            return result;
        }
    }
}