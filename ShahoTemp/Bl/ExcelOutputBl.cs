﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ClosedXML.Excel;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using GropCommonLib;

namespace DensanKyuuyo.Bl
{
    public class ExcelOutputBl
    {
        /// <summary>
        /// 40歳/65歳到達者リストEXCEL作成
        /// </summary>
        /// <returns>処理結果</returns>
        public bool Excel4065yearsold()
        {
            var baseDay = DateTime.Now;

            DataTable xls40 = CommonProcess4065(0, baseDay);
            DataTable xls65 = CommonProcess4065(1, baseDay);

            string filename = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\40歳65歳到達者_" +
                              baseDay.Year.ToString() + "-" + baseDay.Month.ToString() + ".xlsx";

            return CreateExcelFile1(xls40, xls65, filename);
        }

        /// <summary>
        /// 40歳/65歳到達者リストEXCEL作成
        /// </summary>
        /// <param name="age">40歳=0、65歳=1</param>
        /// <param name="baseDay">ベース日付</param>
        /// <returns>データテーブル</returns>
        public DataTable CommonProcess4065(int age, DateTime baseDay)
        {
            var theDay = age == 0 ? baseDay.AddYears(-40) : baseDay.AddYears(-65);

            DateTime start_date = new DateTime(theDay.Year, theDay.Month, 2);       // 当月2日～翌月1日が対象!!!
            theDay = theDay.AddMonths(1);
            DateTime end_date = new DateTime(theDay.Year, theDay.Month, 1);

            ORkyuuyoDBBl bl = new ORkyuuyoDBBl();
            var data_kyuuyojinji = bl.GetInfokyuuyojinjiBybirthdate(start_date, end_date);      // 給与人事マスタ

            var persona = data_kyuuyojinji.Select(x => x.personal_id).ToList();

            var data_kojinsyakaihoken = bl.GetInfokojinsyakaihokenBlock(persona);   // 個人社会保険情報
            var data_kojinkyuuyo = bl.GetInfokojinkyuuyoBlock(persona);             // 個人給与情報
            var data_kyuuyokeisan = bl.GetInfoKyuuyoKeisanBlock(persona);           // 給与計算パラメータ

            DataTable xls = new DataTable();
            xls.Columns.Add("社員コード");
            xls.Columns.Add("氏名");
            xls.Columns.Add("締日");
            xls.Columns.Add("健康保険");
            xls.Columns.Add("介護保険");
            xls.Columns.Add("厚生年金");
            xls.Columns.Add("健保資格取得日");
            xls.Columns.Add("健保資格喪失日");
            xls.Columns.Add("生年月日");
            xls.Columns.Add("40歳到達日");
            xls.Columns.Add("65歳到達日");
            xls.Columns.Add("健康保険番号");
            xls.Columns.Add("会社コード");

            foreach (var emp in data_kyuuyojinji)
            {
                var sya = data_kojinsyakaihoken.FirstOrDefault(x => x.personal_id == emp.personal_id);

                if (sya != null)
                {
                    // 資格取得日が入っており、資格喪失日が入っていない者
                    if (sya.health_start_date != null && sya.health_end_date == null)
                    {
                        DataRow xlsRow = xls.NewRow();
                        xlsRow["社員コード"] = emp.employee_code;
                        xlsRow["氏名"] = emp.last_name + "　" + emp.first_name;
                        xlsRow["生年月日"] = ((DateTime)emp.birth_date).ToString("yyyy/MM/dd");
                        xlsRow["健保資格取得日"] = sya.health_start_date?.ToString("yyyy/MM/dd") ?? string.Empty;
                        xlsRow["健保資格喪失日"] = sya.health_end_date?.ToString("yyyy/MM/dd") ?? string.Empty;
                        xlsRow["健康保険番号"] = sya.health_card_number;
                        xlsRow["会社コード"] = "S-EXP";

                        xlsRow["締日"] = data_kojinkyuuyo.FirstOrDefault(x => x.personal_id == emp.personal_id)?.representative_deadline;

                        var kyukei = data_kyuuyokeisan.FirstOrDefault(x => x.personal_id == emp.personal_id);
                        if (kyukei != null)
                        {
                            xlsRow["健康保険"] = kyukei.health_insurance;
                            xlsRow["厚生年金"] = kyukei.employee_price;
                            xlsRow["介護保険"] = kyukei.care_insurance;
                        }

                        xlsRow["40歳到達日"] = ((DateTime)emp.birth_date).AddYears(40).AddDays(-1).ToString("yyyy/MM/dd");
                        xlsRow["65歳到達日"] = ((DateTime)emp.birth_date).AddYears(65).AddDays(-1).ToString("yyyy/MM/dd");

                        xls.Rows.Add(xlsRow);
                    }
                }
            }

            return xls;
        }

        /// <summary>
        /// 前月20日時点保険加入者EXCEL作成
        /// </summary>
        /// <returns>処理結果</returns>
        public bool ExcelHokenKanyusya()
        {
            var baseDay = DateTime.Now;
            DateTime end_date = new DateTime(baseDay.Year, baseDay.Month, 1);
            end_date = end_date.AddMonths(-1);

            ORkyuuyoDBBl bl = new ORkyuuyoDBBl();
            var data_kojinroudouhoken = bl.GetInfokojinroudouhokenCurrent(end_date);    // 個人労働保険情報

            if (data_kojinroudouhoken == null)
            {
                GMessageUtil.Error("対象データがありません。");
                return false;
            }

            var persona = data_kojinroudouhoken.Select(x => x.personal_id).ToList();
            var employee = data_kojinroudouhoken.Select(x => x.employee_code).ToList();

            var data_kojinsyakaihoken = bl.GetInfokojinsyakaihokenBlock(persona);       // 個人社会保険情報

            BasicInfoBl bbl = new BasicInfoBl();
            var data_basicinfo = bbl.GetBasic_Info_KariTaisyokubi_Block(employee);      // 基本情報 (ローカルDB)

            var data_taisyokusyas = bl.GetInfojinjitaisyokuBlock(persona);              // 人事退職情報

            DataTable xls = new DataTable();
            xls.Columns.Add("社員コード");
            xls.Columns.Add("氏名");
            xls.Columns.Add("健保資格取得日");
            xls.Columns.Add("雇保資格取得日");
            xls.Columns.Add("健保資格喪失日");
            xls.Columns.Add("雇保資格喪失日");
            xls.Columns.Add("短時間区分");
            xls.Columns.Add("仮退職日");
            xls.Columns.Add("退職年月日");
            xls.Columns.Add("短時間社保");

            foreach (var emp in data_kojinroudouhoken)
            {
                DataRow xlsRow = xls.NewRow();
                xlsRow["社員コード"] = emp.employee_code;
                xlsRow["氏名"] = emp.last_name + "　" + emp.first_name;
                xlsRow["雇保資格取得日"] = emp.employment_start_date?.ToString("yyyy/MM/dd") ?? string.Empty;
                xlsRow["雇保資格喪失日"] = emp.employment_end_date?.ToString("yyyy/MM/dd") ?? string.Empty;

                var syaho = data_kojinsyakaihoken.FirstOrDefault(x => x.personal_id == emp.personal_id);
                if (syaho != null)
                {
                    xlsRow["健保資格取得日"] = syaho.employment_start_date?.ToString("yyyy/MM/dd") ?? string.Empty;
                    xlsRow["健保資格喪失日"] = syaho.health_end_date?.ToString("yyyy/MM/dd") ?? string.Empty;

                    switch (syaho.social_short_labor_type)
                    {
                        case 0:
                            xlsRow["短時間社保"] = string.Empty;
                            xlsRow["短時間区分"] = string.Empty;
                            break;

                        case 1:
                            xlsRow["短時間社保"] = "○";
                            xlsRow["短時間区分"] = string.Empty;
                            break;

                        case 2:
                            xlsRow["短時間社保"] = "○";
                            xlsRow["短時間区分"] = "○";
                            break;
                    }
                }

                xlsRow["仮退職日"] = data_basicinfo.FirstOrDefault(x => x.dispatch_worker_code == emp.employee_code)?.kari_taisyokubi?.ToString("yyyy/MM/dd") ?? string.Empty;
                xlsRow["退職年月日"] = data_taisyokusyas.FirstOrDefault(x => x.personal_id == emp.personal_id)?.retirement_date?.ToString("yyyy/MM/dd") ?? string.Empty;

                xls.Rows.Add(xlsRow);
            }

            string filename = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\社保加入者一覧_" +
                              baseDay.Year.ToString() + "-" + baseDay.Month.ToString() + "-" + baseDay.Day.ToString() + ".xlsx";

            return CreateExcelFile2(xls, filename);
        }

        /// <summary>
        /// EXCEL形式のファイルを作成する。
        /// </summary>
        /// <param name="table40">40歳到達者テーブル</param>
        /// <param name="table65">65歳到達者テーブル</param>
        /// <param name="fileName">保存ファイル名</param>
        /// <returns>処理結果</returns>
        private bool CreateExcelFile1(DataTable table40, DataTable table65, string fileName)
        {
            DateTime wksysdate = DateTime.Now;

            try
            {
                using (var wb = new XLWorkbook())
                {
                    IXLWorksheet ws40 = wb.AddWorksheet(wksysdate.Year.ToString() + "-" + wksysdate.Month.ToString() + "_40歳");
                    CreateExcelFile1_Sub(ws40, table40);

                    IXLWorksheet ws65 = wb.AddWorksheet(wksysdate.Year.ToString() + "-" + wksysdate.Month.ToString() + "_65歳");
                    CreateExcelFile1_Sub(ws65, table65);

                    wb.SaveAs(fileName);
                    return true;
                }
            }
            catch (Exception ee)
            {
                GMessageUtil.Error("EXCELファイル作成中にエラーが発生しました。" + "\r\n\r\n" + "コード： " + (ee.HResult & 0x0000FFFF).ToString() +
                                                                                   "\r\n\r\n" + "メッセージ： " + ee.Message);
                return false;
            }
        }

        /// <summary>
        /// EXCEL形式のファイルを作成する（サブ）。
        /// </summary>
        /// <param name="ws">作成するワークシート</param>
        /// <param name="dt">ワークシート作成元のテーブル</param>
        private void CreateExcelFile1_Sub(IXLWorksheet ws, DataTable dt)
        {
            int colIndex = 0;
            foreach (DataColumn col in dt.Columns)
            {
                ws.Cell(1, ++colIndex).Value = col.ColumnName;
            }

            int rowIndex = 2;
            foreach (DataRow row in dt.Rows)
            {
                ws.Cell(rowIndex,  1).SetValue(row["社員コード"]).Style.NumberFormat.Format = "@";
                ws.Cell(rowIndex,  2).SetValue(row["氏名"]);
                ws.Cell(rowIndex,  3).SetValue(row["締日"]);
                ws.Cell(rowIndex,  4).SetValue(row["健康保険"]).Style.NumberFormat.Format = "#,##0";
                ws.Cell(rowIndex,  5).SetValue(row["介護保険"]).Style.NumberFormat.Format = "#,##0";
                ws.Cell(rowIndex,  6).SetValue(row["厚生年金"]).Style.NumberFormat.Format = "#,##0";
                ws.Cell(rowIndex,  7).SetValue(row["健保資格取得日"]);
                ws.Cell(rowIndex,  8).SetValue(row["健保資格喪失日"]);
                ws.Cell(rowIndex,  9).SetValue(row["生年月日"]);
                ws.Cell(rowIndex, 10).SetValue(row["40歳到達日"]);
                ws.Cell(rowIndex, 11).SetValue(row["65歳到達日"]);
                ws.Cell(rowIndex, 12).SetValue(row["健康保険番号"]);
                ws.Cell(rowIndex, 13).SetValue(row["会社コード"]);

                ws.Cell(rowIndex,  3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                ws.Cell(rowIndex,  4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                ws.Cell(rowIndex,  5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                ws.Cell(rowIndex,  6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                ws.Cell(rowIndex, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                rowIndex++;
            }

            SetExcelSheetFormat(ws);
        }

        /// <summary>
        /// EXCEL形式のファイルを作成する。
        /// </summary>
        /// <param name="table">保険加入者テーブル</param>
        /// <param name="fileName">保存ファイル名</param>
        /// <returns>処理結果</returns>
        private bool CreateExcelFile2(DataTable table, string fileName)
        {
            DateTime wksysdate = DateTime.Now;

            try
            {
                using (var wb = new XLWorkbook())
                {
                    // ワークシートの設定
                    IXLWorksheet ws = wb.AddWorksheet(wksysdate.Year.ToString() + "-" + wksysdate.Month.ToString() + "-" + wksysdate.Day.ToString());

                    int colIndex = 0;
                    foreach (DataColumn col in table.Columns)
                    {
                        ws.Cell(1, ++colIndex).Value = col.ColumnName;
                    }

                    int rowIndex = 2;
                    foreach (DataRow row in table.Rows)
                    {
                        ws.Cell(rowIndex,  1).SetValue(row["社員コード"]).Style.NumberFormat.Format = "@";
                        ws.Cell(rowIndex,  2).SetValue(row["氏名"]);
                        ws.Cell(rowIndex,  3).SetValue(row["健保資格取得日"]);
                        ws.Cell(rowIndex,  4).SetValue(row["雇保資格取得日"]);
                        ws.Cell(rowIndex,  5).SetValue(row["健保資格喪失日"]);
                        ws.Cell(rowIndex,  6).SetValue(row["雇保資格喪失日"]);
                        ws.Cell(rowIndex,  7).SetValue(row["短時間区分"]);
                        ws.Cell(rowIndex,  8).SetValue(row["仮退職日"]);
                        ws.Cell(rowIndex,  9).SetValue(row["退職年月日"]);
                        ws.Cell(rowIndex, 10).SetValue(row["短時間社保"]);

                        ws.Cell(rowIndex,  7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell(rowIndex, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        rowIndex++;
                    }

                    SetExcelSheetFormat(ws);

                    wb.SaveAs(fileName);

                    return true;
                }
            }
            catch (Exception ee)
            {
                GMessageUtil.Error("EXCELファイル作成中にエラーが発生しました。" + "\r\n\r\n" + "コード： " + (ee.HResult & 0x0000FFFF).ToString() +
                                                                                   "\r\n\r\n" + "メッセージ： " + ee.Message);
                return false;
            }
        }

        /// <summary>
        /// 出力Excelの書式設定をする。
        /// </summary>
        /// <param name="ws">シートオブジェクト</param>
        private void SetExcelSheetFormat(IXLWorksheet ws)
        {
            // フォント
            ws.Style.Font.FontName = "ＭＳ Ｐゴシック";

            // 文字表示位置 上
            ws.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;

            // 列幅自動調整
            ws.ColumnsUsed().AdjustToContents();

            // 罫線
            ws.Range(ws.Cell("A1").Address, ws.LastCellUsed().Address).Style
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin);

            // 項目名ヘッダー　色付け
            ws.Range(ws.Cell(1, 1), ws.Cell(1, ws.LastCellUsed().Address.ColumnNumber)).Style.Fill.BackgroundColor = XLColor.SkyBlue;
        }
    }
}
