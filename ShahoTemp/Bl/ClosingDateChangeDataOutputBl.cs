﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ClosedXML.Excel;
using DensanKyuuyo.Model;
using GropCommonLib;
using System.Diagnostics;

namespace DensanKyuuyo.Bl
{
    public class ClosingDateChangeDataOutputBl
    {
        private List<string> persona;

        /// <summary>
        /// 締日別増減データ作成
        /// </summary>
        /// <param name="inputExcel">入力EXCELブック名</param>
        public void PutList(string inputExcel)
        {
            PutListD(inputExcel);
            PutListG(inputExcel);
            PutListJ(inputExcel);
        }

        [Conditional("DEBUG")]
        private void PutListD(string inputExcel)
        {
            persona = new List<string>();
            var listG = GetHenkousyaList(inputExcel, "G");
            PutZougenList(listG, 1);
            PutZougenList(listG, 2);
        }

        [Conditional("RELEASE_GROP")]
        private void PutListG(string inputExcel)
        {
            persona = new List<string>();
            var listG = GetHenkousyaList(inputExcel, "G");
            PutZougenList(listG, 1);
            PutZougenList(listG, 2);
        }

        [Conditional("RELEASE_JOY")]
        private void PutListJ(string inputExcel)
        {
            persona = new List<string>();
            var listJ = GetHenkousyaList(inputExcel, "J");
            PutZougenList(listJ, 3);
            PutZougenList(listJ, 4);
        }

        /// <summary>
        /// 入力EXCELを読み込んでリストに格納
        /// </summary>
        /// <param name="inputExcel">入力EXCELブック名</param>
        /// <param name="syubetsu">"G"=GROP / "J"=JOY</param>
        /// <returns>リスト</returns>
        public List<ClosingDateChangeDataOutputModel> GetHenkousyaList(string inputExcel, string syubetsu)
        {
            var list = new List<ClosingDateChangeDataOutputModel>();

            try
            {
                using (var workbookin = new XLWorkbook(inputExcel))
                {
                    var wsin = syubetsu == "G" ? workbookin.Worksheet("加入G") : workbookin.Worksheet("加入J");

                    for (int row = 5; row <= wsin.LastRowUsed().RowNumber(); row++)
                    {
                        var listrow = new ClosingDateChangeDataOutputModel();

                        listrow.id            = wsin.Cell("B" + row.ToString()).Value.ToString();
                        listrow.name          = wsin.Cell("C" + row.ToString()).Value.ToString();
                        listrow.before_change = wsin.Cell("D" + row.ToString()).Value.ToString();
                        listrow.after_change  = wsin.Cell("E" + row.ToString()).Value.ToString();

                        persona.Add(listrow.id);

                        list.Add(listrow);
                    }
                }

                ORkyuuyoDBBl bl = new ORkyuuyoDBBl();
                List<ORjinji> jinjis = bl.GetInfojinjiBlock(persona);       // 人事マスタで社員コードを個人IDに変換

                foreach (var member in list)
                {
                    member.personal_id = jinjis.FirstOrDefault(x => x.employee_code == member.id)?.personal_id;  // listに個人IDをセット
                }

                // 社会保険料セット
                List<string> persona2 = jinjis.Select(x => x.personal_id).ToList();
                List<ORkyuuyoKeisan> kyuuyoKeisan = bl.GetInfoKyuuyoKeisanBlock(persona2);

                foreach (var obj in list)
                {
                    var kyukei = kyuuyoKeisan.FirstOrDefault(x => x.personal_id == obj.personal_id);
                    if (kyukei != null)
                    {
                        obj.kenkou_hokenryou = kyukei.health_insurance;
                        obj.kousei_nenkin = kyukei.employee_price;
                        obj.kaigo_hokenryou = kyukei.care_insurance;
                    }
                }
            }
            catch (Exception ee)
            {
                switch (ee.HResult & 0x0000FFFF)
                {
                    case 32:
                        GMessageUtil.Halt("ファイルが別のプロセスで使用されています。");
                        return null;
                    case 87:
                        string wsname = syubetsu == "G" ? "加入G" : "加入J";
                        GMessageUtil.Halt("入力ブックにワークシート「" + wsname + "」が見つかりません。");
                        return null;
                    default:
                        GMessageUtil.Halt((ee.HResult & 0x0000FFFF).ToString() + "\r\n" + ee.Message);
                        return null;
                }
            }

            return list;
        }

        /// <summary>
        /// ワークブック書き込み
        /// </summary>
        /// <param name="list">入力リスト</param>
        /// <param name="syubetsu">1=GROP減 / 2=GROP増 / 3=JOY減 / 4=JOY増</param>
        public void PutZougenList(List<ClosingDateChangeDataOutputModel> list, int syubetsu)
        {
            IXLWorksheet wsout15;
            IXLWorksheet wsout20;
            IXLWorksheet wsout25;
            IXLWorksheet wsout末;

            try
            {
                using (var workbookout = new XLWorkbook())
                {
                    if (syubetsu == 1 || syubetsu == 3)
                    {
                        wsout15 = workbookout.Worksheets.Add("_15締マイナス");
                        wsout20 = workbookout.Worksheets.Add("_20締マイナス");
                        wsout25 = workbookout.Worksheets.Add("_25締マイナス");
                        wsout末 = workbookout.Worksheets.Add("_末締マイナス");
                    }
                    else
                    {
                        wsout15 = workbookout.Worksheets.Add("_15締プラス");
                        wsout20 = workbookout.Worksheets.Add("_20締プラス");
                        wsout25 = workbookout.Worksheets.Add("_25締プラス");
                        wsout末 = workbookout.Worksheets.Add("_末締プラス");
                    }

                    PutZougenListSubH(wsout15);
                    PutZougenListSubH(wsout20);
                    PutZougenListSubH(wsout25);
                    PutZougenListSubH(wsout末);

                    int row15 = 2;
                    int row20 = 2;
                    int row25 = 2;
                    int row末 = 2;

                    foreach (var outdata in list)
                    {
                        switch (syubetsu == 1 || syubetsu == 3 ? outdata.before_change : outdata.after_change)
                        {
                            case "15":
                                PutZougenListSubD(outdata, wsout15, row15);
                                row15++;
                                break;

                            case "20":
                                PutZougenListSubD(outdata, wsout20, row20);
                                row20++;
                                break;

                            case "25":
                                PutZougenListSubD(outdata, wsout25, row25);
                                row25++;
                                break;

                            case "末":
                                PutZougenListSubD(outdata, wsout末, row末);
                                row末++;
                                break;

                            default:
                                continue;
                        }
                    }

                    SetExcelSheetFormat(wsout15);
                    SetExcelSheetFormat(wsout20);
                    SetExcelSheetFormat(wsout25);
                    SetExcelSheetFormat(wsout末);

                    string foldername = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

                    switch (syubetsu)
                    {
                        case 1:
                        case 3:
                            workbookout.SaveAs(foldername + @"\締日別_社保料増減一覧_減.xlsx");
                            break;
                        case 2:
                        case 4:
                            workbookout.SaveAs(foldername + @"\締日別_社保料増減一覧_増.xlsx");
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ee)
            {
                switch (ee.HResult & 0x0000FFFF)
                {
                    default:
                        GMessageUtil.Halt((ee.HResult & 0x0000FFFF).ToString() + "\r\n" + ee.Message);
                        return;
                }
            }
        }

        /// <summary>
        /// シートヘッダ部書き込み
        /// </summary>
        /// <param name="wsout">ワークシート</param>
        public void PutZougenListSubH(IXLWorksheet wsout)
        {
            wsout.Cell("A1").Value = "社員コード";
            wsout.Cell("B1").Value = "氏名";
            wsout.Cell("C1").Value = "変更前";
            wsout.Cell("D1").Value = "変更後";
            wsout.Cell("E1").Value = "健康保険";
            wsout.Cell("F1").Value = "介護保険";
            wsout.Cell("G1").Value = "厚生年金";
        }

        /// <summary>
        /// シート明細部書き込み
        /// </summary>
        /// <param name="cd">リスト</param>
        /// <param name="wsout">ワークシート</param>
        /// <param name="row">書き込む行番号</param>
        public void PutZougenListSubD(ClosingDateChangeDataOutputModel cd, IXLWorksheet wsout, int row)
        {
            wsout.Cell("A" + row.ToString()).SetValue(cd.id);
            wsout.Cell("B" + row.ToString()).SetValue(cd.name);
            wsout.Cell("C" + row.ToString()).SetValue(cd.before_change);
            wsout.Cell("D" + row.ToString()).SetValue(cd.after_change);
            wsout.Cell("E" + row.ToString()).SetValue(cd.kenkou_hokenryou).Style.NumberFormat.Format = "#,##0";
            wsout.Cell("F" + row.ToString()).SetValue(cd.kaigo_hokenryou).Style.NumberFormat.Format = "#,##0";
            wsout.Cell("G" + row.ToString()).SetValue(cd.kousei_nenkin).Style.NumberFormat.Format = "#,##0";
        }

        /// <summary>
        /// 出力Excelの書式設定をする。
        /// </summary>
        /// <param name="ws">シートオブジェクト</param>
        private void SetExcelSheetFormat(IXLWorksheet ws)
        {
            // フォント
            ws.Style.Font.FontName = "ＭＳ Ｐゴシック";

            // 文字表示位置
            ws.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            // 列幅自動調整
            ws.ColumnsUsed().AdjustToContents();

            // 行の高さ
            ws.RowHeight = 20;

            // 罫線
            ws.Range(ws.Cell("A1").Address, ws.LastCellUsed().Address).Style
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin);

            // 項目名ヘッダー　色付け
            ws.Range(ws.Cell(1, 1), ws.Cell(1, ws.LastCellUsed().Address.ColumnNumber)).Style.Fill.BackgroundColor = XLColor.DarkGray;
        }
    }
}
