﻿using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System.Collections.Generic;
using System.Linq;

namespace DensanKyuuyo.Bl
{
    public class DispatchWorkerForOrdiaBl
    {
        /// <summary>
        /// スタッフME紐づけリストとスタッフリストのJOIN
        /// </summary>
        /// <param name="sflist">スタッフリスト(担当者コードなし)</param>
        /// <returns>スタッフリスト(担当者コード入り)</returns>
        public List<DispatchWorker> GetStaffList(List<DispatchWorker> sflist)
        {
            // スタッフME紐づけビューの読み込み
            // 同一スタッフで複数レコードある可能性があるため、開始日が最新のレコードを読むようにする
            var smlist = OrdiaMSqlMapper.GetInstance().QueryForList<DispatchWorkerEmployee>("SelectStaffAndMEAll", null).ToList();

            var query = sflist
                           .GroupJoin(
                                  smlist,
                                  f => f.dispatch_worker_code,
                                  m => m.dispatch_worker_code,
                                  (sf, sm) => new DispatchWorker
                                  {
                                      dispatch_worker_code = sf.dispatch_worker_code,
                                      name = sf.name,
                                      name_kana = sf.name_kana,
                                      health_insurance_number = sf.health_insurance_number,
                                      department_name = sf.department_name,
                                      health_insurance_join_date = sf.health_insurance_join_date,
                                      health_insurance_loss_date = sf.health_insurance_loss_date,
                                      employment_insurance_join_date = sf.employment_insurance_join_date,
                                      employment_insurance_loss_date = sf.employment_insurance_loss_date,
                                      standard_monthly_remuneration = sf.standard_monthly_remuneration,
                                      nursing_insurance_flag = sf.nursing_insurance_flag,
                                      manager_code = sm.Any() ? sm.FirstOrDefault().employee_code : string.Empty
                                  });

            return query.ToList();
        }

        /// <summary>
        /// スタッフME未紐づけ情報にME情報を付加
        /// </summary>
        /// <param name="sfinfo">スタッフ情報(担当者コードなし)</param>
        /// <returns>スタッフ情報(担当者コード入り)</returns>
        public DispatchWorker GetStaffInfo(DispatchWorker sfinfo)
        {
            // スタッフME紐づけビューの読み込み
            // 同一スタッフで複数レコードある可能性があるため、開始日が最新のレコードを読むようにする
            var emp = OrdiaMSqlMapper.GetInstance().QueryForObject<DispatchWorkerEmployee>("SelectStaffAndME", sfinfo);

            sfinfo.manager_code = emp != null ? emp.employee_code : string.Empty;

            return sfinfo;
        }
    }
}
