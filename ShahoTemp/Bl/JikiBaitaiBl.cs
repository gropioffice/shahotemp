﻿using System;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System.Collections.Generic;
using System.Linq;
using GropCommonLib;
using System.Windows.Forms;
using System.Threading;
using CsvHelper;
using System.IO;
using System.Text;

namespace DensanKyuuyo.Bl
{
    public class JikiBaitaiBl
    {
        /// <summary>
        /// 磁気媒体CSV作成
        /// </summary>
        /// <param name="list">社員リスト</param>
        /// <param name="kenpokoho">健保雇保区分：1=健保／2=雇保</param>
        /// <returns>CSV作成成否</returns>
        public bool CreateJikiBaitaiCSV(List<JikiBaitaiEmployee> list, int kenpokoho)
        {
            string dtpath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

            switch (kenpokoho)
            {
                case 1:
                    dtpath += @"\磁気媒体_健保.csv";
                    break;
                case 2:
                    dtpath += @"\磁気媒体_雇保.csv";
                    break;
                default:
                    GMessageUtil.Error("プログラムエラー：　健保雇保区分異常！");
                    return false;
            }

            var templist = MappingJikiBaitaiModel(list, kenpokoho);
            return OutputJikiBaitaiCSV(dtpath, templist);
        }

        /// <summary>
        /// 磁気媒体CSVデータ出力
        /// </summary>
        /// <param name="filePath">ファイル名を含んだファイルパス</param>
        /// <param name="outputList">磁気媒体モデルのリスト</param>
        /// <returns>CSV作成成否</returns>
        private static bool OutputJikiBaitaiCSV(string filePath, List<JikiBaitaiModel> outputList)
        {
            try
            {
                // CSVファイルの出力 (shift-jis)
                using (var sw = new StreamWriter(filePath, false, Encoding.GetEncoding("shift-jis")))
                using (var csvWriter = new CsvWriter(sw))
                {
                    csvWriter.Configuration.RegisterClassMap<JikiBaitaiMapper>();
                    csvWriter.Configuration.HasHeaderRecord = false;                // ヘッダーなし
                    csvWriter.WriteRecords(outputList);
                }

                return true;
            }
            catch (Exception ee)
            {
                GMessageUtil.Error("CSVファイルが作成できませんでした。" + "\r\n" + "\r\n" + ee.ToString());
                return false;
            }
        }

        /// <summary>
        /// 磁気媒体モデルへのマッピング
        /// </summary>
        /// <param name="employee_list">社員リスト</param>
        /// <param name="kenpokoho">健保雇保区分：1=健保／2=雇保</param>
        /// <returns>磁気媒体モデルのリスト</returns>
        private static List<JikiBaitaiModel> MappingJikiBaitaiModel(List<JikiBaitaiEmployee> employee_list, int kenpokoho)
        {
            List<JikiBaitaiModel> outputList = new List<JikiBaitaiModel>();

            foreach (var employee in employee_list)
            {
                ORkyuuyoDBBl bl = new ORkyuuyoDBBl();

                var data_jinji = bl.GetInfojinji(employee.dispatch_worker_code);        // 人事マスタ
                string personal_id = data_jinji.personal_id;

                var data_kyuuyojinji = bl.GetInfokyuuyojinji(personal_id);              // 給与人事マスタ
                var data_kojinsyakaihoken = bl.GetInfokojinsyakaihoken(personal_id);    // 個人社会保険情報
                var data_kojinjyuusyo = bl.GetInfokojinjyuusyo(personal_id);            // 個人住所情報
                var data_kojinroudouhoken = bl.GetInfokojinroudouhoken(personal_id);    // 個人労働保険情報
                var data_kaiteiDataKenpo = bl.GetInfokaiteiDataKenpo(personal_id);      // 健康保険改定データ
                var data_kaiteiDataKounen = bl.GetInfokaiteiDataKounen(personal_id);    // 厚生年金改定データ

                BasicInfoBl bbl = new BasicInfoBl();

                var data_basicinfo = bbl.GetBasic_Info(employee.dispatch_worker_code);

                if (data_jinji == null ||
                    data_kyuuyojinji == null ||
                    data_kojinsyakaihoken == null ||
                    data_kojinjyuusyo == null ||
                    data_kojinroudouhoken == null ||
                    data_kaiteiDataKenpo == null ||
                    data_kaiteiDataKounen == null ||
                    data_basicinfo == null)
                {
                    continue;
                }

                var outputData1 = new JikiBaitaiModel();

                // 雇保の場合はヘッダを編集
                if (kenpokoho == 2)
                {
                    //TekiyouJigyosyoBl tbl = new TekiyouJigyosyoBl();
                    //var jigyosyo = tbl.GetInfojigyosyo((int)data_basicinfo.tekiyou_jigyousyo_code);

                    //if (jigyosyo != null)
                    //{
                    //    outputData1.事業所整理記号＿都道府県コード = jigyosyo.prefecture_code;
                    //    outputData1.事業所整理記号＿事業所記号 = jigyosyo.jigyosyo_name;

                    if (data_basicinfo.tekiyou_jigyousyo_code != null)
                    {
                        SqlServerBl sbl = new SqlServerBl();
                        var data_jigyosyo = sbl.GetInfoJigyosyo((int)data_basicinfo.tekiyou_jigyousyo_code);

                        if (data_jigyosyo != null)
                        {
                            outputData1.事業所整理記号＿都道府県コード = data_jigyosyo.都道府県コード ?? "59";
                            outputData1.事業所整理記号＿事業所記号 = data_jigyosyo.事業所名カナ略称;
                        }
                    }
                }

                outputData1.被保険者整理番号 = "9" + employee.dispatch_worker_code.Substring(employee.dispatch_worker_code.Length - 5);
                outputData1.被保険者氏名＿カナ = data_jinji.last_kana + " " + data_jinji.first_kana;
                outputData1.被保険者氏名＿漢字 = data_jinji.last_name + "　" + data_jinji.first_name;
                outputData1.生年月日＿元号 = DataCnv(1, (DateTime)data_kyuuyojinji.birth_date);
                outputData1.生年月日＿年月日 = DataCnv(2, (DateTime)data_kyuuyojinji.birth_date);
                outputData1.種別＿性別 = data_kyuuyojinji.gender_type.ToString();
                outputData1.基礎年金番号＿課所符号 = data_kojinsyakaihoken.basic_pension_number.Substring(0, 4);
                outputData1.基礎年金番号＿一連番号 = data_kojinsyakaihoken.basic_pension_number.Substring(4, 6);
                outputData1.郵便番号＿親番号 = data_kojinjyuusyo.postal_code_1;
                outputData1.郵便番号＿子番号 = data_kojinjyuusyo.postal_code_2;
                outputData1.被保険者住所＿カナ = data_kojinjyuusyo.address_kana;
                outputData1.被保険者住所＿漢字 = data_kojinjyuusyo.address_1 + data_kojinjyuusyo.address_2 + data_kojinjyuusyo.address_3;

                // 健保・雇保で取得先が変わる
                if (kenpokoho == 1)
                {
                    outputData1.資格取得年月日＿元号 = DataCnv(1, (DateTime)data_kojinsyakaihoken.health_start_date);
                    outputData1.資格取得年月日＿年月日 = DataCnv(2, (DateTime)data_kojinsyakaihoken.health_start_date);
                }
                else
                {
                    outputData1.資格取得年月日＿元号 = DataCnv(1, (DateTime)data_kojinroudouhoken.employment_start_date);
                    outputData1.資格取得年月日＿年月日 = DataCnv(2, (DateTime)data_kojinroudouhoken.employment_start_date);
                }

                outputData1.従前の標準報酬月額＿健保 = ((int)data_kaiteiDataKenpo.basic_monthly / 1000).ToString("0000");
                outputData1.従前の標準報酬月額＿厚年 = ((int)data_kaiteiDataKounen.basic_monthly / 1000).ToString("0000");

                outputData1.従前の改定月＿元号 = "7";
                outputData1.従前の改定月＿年 = "20";
                outputData1.従前の改定月＿月 = "09";
                //outputData1.従前の改定月＿元号 = DataCnv(1, (DateTime)data_kaiteiDataKenpo.revision_date);
                //outputData1.従前の改定月＿年 = DataCnv(2, (DateTime)data_kaiteiDataKenpo.revision_date).Substring(0, 2);
                //outputData1.従前の改定月＿月 = DataCnv(2, (DateTime)data_kaiteiDataKenpo.revision_date).Substring(2, 2);

                outputData1.被保険者証番号 = data_kojinsyakaihoken.health_card_number;

                outputData1.算定＿昇降給区分 = "2";
                outputData1.算定＿昇降給差の月額 = 0;
                outputData1.算定＿昇降給月＿年 = "21";
                outputData1.算定＿昇降給月＿月 = "04";
                outputData1.算定＿備考欄 = "ｶｯﾄ";
                outputData1.賞与＿通貨によるものの額 = 0;
                outputData1.賞与＿現物によるものの額 = 0;

                outputData1.雇用保険被保険者番号４桁 = data_kojinroudouhoken.employment_number.Substring(0, 4);
                outputData1.雇用保険被保険者番号６桁 = data_kojinroudouhoken.employment_number.Substring(4, 6);
                outputData1.雇用保険被保険者番号ＣＤ = data_kojinroudouhoken.employment_number.Substring(10, 1);
                outputData1.性別 = data_kyuuyojinji.gender_type.ToString();
                outputData1.一週間の所定労働時間＿時間 = data_kojinroudouhoken.specific_work_time_hours;
                outputData1.一週間の所定労働時間＿分 = data_kojinroudouhoken.specific_work_time_minutes;

                outputList.Add(outputData1);
            }

            return outputList;
        }

        /// <summary>
        /// 西暦和暦変換
        /// </summary>
        /// <param name="mode">モード</param>
        /// <param name="sdate">変換対象年月日(西暦)</param>
        /// <returns>mode=1の時、元号コード。mode=2の時、和暦年月日</returns>
        /// <remarks>元号コード: 1=明治、3=大正、5=昭和、7=平成、9=令和</remarks>
        private static string DataCnv(int mode, DateTime sdate)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ja-JP", false);
            ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            var wareki = sdate.ToString("ggyyMMdd", ci);

            switch (mode)
            {
                // 元号コードを返す
                case 1:
                    switch (wareki.Substring(0, 2))
                    {
                        case "明治":
                            return "1";
                        case "大正":
                            return "3";
                        case "昭和":
                            return "5";
                        case "平成":
                            return "7";
                        case "令和":
                            return "9";
                        default:
                            return string.Empty;
                    }

                // 年月日を返す
                case 2:
                    return wareki.Substring(2, 6);

                default:
                    return string.Empty;
            }
        }
    }
}