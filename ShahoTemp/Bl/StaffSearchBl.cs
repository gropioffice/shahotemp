﻿using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System.Collections.Generic;
using System.Linq;

namespace DensanKyuuyo.Bl
{
    public class StaffSearchBl
    {
        /// <summary>
        /// 検索条件に基づき派遣社員情報を取得
        /// </summary>
        /// <param name="dispatchWorkerCode">社員コード</param>
        /// <param name="name">社員名</param>
        /// <param name="nameKana">社員名カナ</param>
        /// <param name="healthInsuranceNumber">健康保険番号</param>
        /// <returns>検索結果</returns>
        public List<DispatchWorker> FetchStaffInfo(string dispatchWorkerCode, string name, string nameKana, string healthInsuranceNumber)
        {
            DispatchWorker param = new DispatchWorker();
            param.dispatch_worker_code = dispatchWorkerCode;
            param.name = name;
            param.name_kana = nameKana;
            param.health_insurance_number = healthInsuranceNumber;

            // 派遣社員情報を取得
            var temp = OrdiaSqlMapper.GetInstance().QueryForList<DispatchWorker>("SelectDispatchWorkerInfo", param).ToList();
            var bl = new DispatchWorkerForOrdiaBl();
            var dispatchWorkerInfos = bl.GetStaffList(temp);

            // 社員マスタを取得
            var employeeInfos = OrdiaMSqlMapper.GetInstance().QueryForList<Employee>("SelectEmployeeAll", null).ToList();

            var searchResultList = dispatchWorkerInfos
                          .GroupJoin(
                                 employeeInfos,
                                 d => d.manager_code,
                                 e => e.employee_code,
                                 (dispatch, employee) => new DispatchWorker
                                 {
                                     dispatch_worker_code = dispatch.dispatch_worker_code,
                                     name = dispatch.name ?? string.Empty,
                                     name_kana = dispatch.name_kana ?? string.Empty,
                                     health_insurance_number = dispatch.health_insurance_number,
                                     manager_code = dispatch.manager_code,
                                     manager_name = employee.Any() ? employee.FirstOrDefault().name : string.Empty,
                                     manager_mail_address = employee.Any() ? employee.FirstOrDefault().mail_address : string.Empty
                                 });

            return searchResultList.ToList();
        }
    }
}
