﻿using IBatisNet.Common;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DensanKyuuyo.Bl
{
    public class NoticeInputBl
    {
        /// <summary>
        /// 保留内容を取得する。
        /// </summary>
        /// <returns>保留理由マスタリスト</returns>
        public List<DeferReasonMaster> GetAllDeferReasonMaster()
        {
            // 保留理由マスタの取得
            var list = GropShareSqlMapper.GetInstance().QueryForList<DeferReasonMaster>("SelectDeferReasonMasterAll", null).ToList();

            // ソートして返す
            return list;
        }

        /// <summary>
        /// お知らせを登録する。
        /// </summary>
        /// <param name="notice">登録リスト</param>
        /// <returns>登録結果</returns>
        public bool NoticeInsert(Notice notice)
        {
            using (IDalSession session = GropShareSqlMapper.GetInstance().BeginTransaction())
            {
                Notice nt = new Notice();
                nt = GropShareSqlMapper.GetInstance().QueryForObject<Notice>("SelectNoticeByDispatchWorkerCode", notice);
                if (nt != null)
                {
                    session.Complete();
                    return false;                   // 他で登録された
                }

                GropShareSqlMapper.GetInstance().Insert("InsertNotice", notice);
                session.Complete();
                return true;
            }
        }

        /// <summary>
        /// お知らせを更新する。
        /// </summary>
        /// <param name="notice">登録リスト</param>
        /// <param name="lastupdateat">最終更新日時</param>
        /// <returns>更新結果</returns>
        public bool NoticeUpdate(Notice notice, DateTime? lastupdateat)
        {
            using (IDalSession session = GropShareSqlMapper.GetInstance().BeginTransaction())
            {
                Notice nt = new Notice();
                nt = GropShareSqlMapper.GetInstance().QueryForObject<Notice>("SelectNoticeByDispatchWorkerCode", notice);
                if (nt == null)
                {
                    session.Complete();
                    return false;                   // レコードが削除された
                }

                if (nt.updated_at != lastupdateat)
                {
                    session.Complete();
                    return false;                   // 他で更新された
                }

                GropShareSqlMapper.GetInstance().Update("UpdateNotice", notice);
                session.Complete();
                return true;
            }
        }

        /// <summary>
        /// お知らせを一件検索する。
        /// </summary>
        /// <param name="notice">検索リスト</param>
        /// <returns>検索結果</returns>
        public Notice NoticeSelect(Notice notice)
        {
            Notice nt = new Notice();
            nt = GropShareSqlMapper.GetInstance().QueryForObject<Notice>("SelectNoticeByDispatchWorkerCode", notice);

            return nt;
        }
    }
}