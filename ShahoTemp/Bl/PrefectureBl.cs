﻿using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;

namespace DensanKyuuyo.Bl
{
    public class PrefectureBl
    {
        /// <summary>
        /// 都道府県マスタからデータ取得
        /// </summary>
        /// <param name="prefecture_code">都道府県コード</param>
        /// <returns>検索結果</returns>
        public Prefecture GetInfoprefecture(string prefecture_code)
        {
            switch (prefecture_code)
            {
                case "1":
                    prefecture_code = "01";
                    break;
                case "2":
                    prefecture_code = "02";
                    break;
                case "3":
                    prefecture_code = "03";
                    break;
                case "4":
                    prefecture_code = "04";
                    break;
                case "5":
                    prefecture_code = "05";
                    break;
                case "6":
                    prefecture_code = "06";
                    break;
                case "7":
                    prefecture_code = "07";
                    break;
                case "8":
                    prefecture_code = "08";
                    break;
                case "9":
                    prefecture_code = "09";
                    break;
            }

            Prefecture param = new Prefecture();
            param.prefecture_code = prefecture_code;

            return GropShareSqlMapper.GetInstance().QueryForObject<Prefecture>("SelectPrefecture", param);
        }
    }
}
