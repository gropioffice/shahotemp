﻿using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System.Collections.Generic;
using System.Linq;

namespace DensanKyuuyo.Bl
{
    public class ProgressDialogBl
    {
        /// <summary>
        /// 保留理由マスタリストを取得する。
        /// </summary>
        /// <returns>保留理由マスタリスト</returns>
        public List<DeferReasonMaster> GetAllDeferReasonMaster()
        {
            return GropShareSqlMapper.GetInstance().QueryForList<DeferReasonMaster>("SelectDeferReasonMasterAll", null).ToList();
        }

        /// <summary>
        /// 派遣社員リストを取得する。
        /// </summary>
        /// <param name="list">送信対象のお知らせリスト</param>
        /// <returns>派遣社員リスト</returns>
        public List<DispatchWorker> GetAllDispatchWorker(List<Notice> list)
        {
            var dispatchWorkerRepository = new Common.DispatchWorkerRepository();
            return dispatchWorkerRepository.GetDispatchWorkers(list.Select(x => x.dispatch_worker_code).ToList());
        }

        /// <summary>
        /// 社員リストを取得する。
        /// </summary>
        /// <returns>社員リスト</returns>
        public List<Employee> GetAllEmployee()
        {
            return OrdiaMSqlMapper.GetInstance().QueryForList<Employee>("SelectAllEmployee", null).ToList();
        }
    }
}
