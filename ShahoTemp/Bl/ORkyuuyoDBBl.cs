﻿using GropCommonLib;
using DensanKyuuyo.Common;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DensanKyuuyo.Bl
{
    public class ORkyuuyoDBBl
    {
        /// <summary>
        /// ORDIA_人事マスタからデータ取得
        /// </summary>
        /// <param name="dispatchWorkerCode">個人ID</param>
        /// <returns>検索結果</returns>
        public ORjinji GetInfojinji(string dispatchWorkerCode)
        {
            ORjinji param = new ORjinji();
            param.employee_code = dispatchWorkerCode;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORjinji>("SelectORjinji", param);
        }

        /// <summary>
        /// ORDIA_人事マスタで社員コードを個人IDに変換（締日EXCEL作成用）
        /// </summary>
        /// <param name="emplo">社員コードリスト</param>
        /// <returns>検索結果</returns>
        public List<ORjinji> GetInfojinjiBlock(List<string> emplo)
        {
            var result = new List<ORjinji>();

            foreach (var codes in emplo.Chunk(Constants.PostgresInClauseMaxCount))
            {
                var conditions = new Hashtable();
                conditions.Add("employee_codes", codes.ToArray());

                var data_jinjis = OrdiaSqlMapper.GetInstance().QueryForList<ORjinji>("SelectORjinjiBlock", conditions).ToList();
                result.AddRange(data_jinjis);
            }

            return result;
        }

        /// <summary>
        /// ORDIA_給与人事マスタからデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORkyuuyojinji GetInfokyuuyojinji(string personalID)
        {
            ORkyuuyojinji param = new ORkyuuyojinji();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkyuuyojinji>("SelectORkyuuyojinji", param);
        }

        /// <summary>
        /// ORDIA_給与人事マスタ（と人事マスタ）から誕生日がある範囲にあるデータ取得（EXCEL作成用）
        /// </summary>
        /// <param name="start_date">選択開始日</param>
        /// <param name="end_date">選択終了日</param>
        /// <returns>検索結果</returns>
        public List<ORkyuuyojinji> GetInfokyuuyojinjiBybirthdate(DateTime start_date, DateTime end_date)
        {
            ORkyuuyojinji param = new ORkyuuyojinji();
            param.start_date = start_date;
            param.end_date = end_date;

            return OrdiaSqlMapper.GetInstance().QueryForList<ORkyuuyojinji>("SelectORkyuuyojinjiBybirthdate", param).ToList();

        }

        /// <summary>
        /// ORDIA_個人住所情報からデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORkojinjyuusyo GetInfokojinjyuusyo(string personalID)
        {
            ORkojinjyuusyo param = new ORkojinjyuusyo();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkojinjyuusyo>("SelectORkojinjyuusyo", param);
        }

        /// <summary>
        /// ORDIA_人事入社情報からデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORjinjinyuusya GetInfojinjinyuusya(string personalID)
        {
            ORjinjinyuusya param = new ORjinjinyuusya();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORjinjinyuusya>("SelectORjinjinyuusya", param);

        }

        /// <summary>
        /// ORDIA_人事退職情報からデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORjinjitaisyoku GetInfojinjitaisyoku(string personalID)
        {
            ORjinjitaisyoku param = new ORjinjitaisyoku();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORjinjitaisyoku>("SelectORjinjitaisyoku", param);
        }

        /// <summary>
        /// ORDIA_人事退職情報からデータ取得（EXCEL作成用）
        /// </summary>
        /// <param name="persona">個人IDリスト</param>
        /// <returns>検索結果</returns>
        public List<ORjinjitaisyoku> GetInfojinjitaisyokuBlock(List<string> persona)
        {
            var result = new List<ORjinjitaisyoku>();

            foreach (var codes in persona.Chunk(Constants.PostgresInClauseMaxCount))
            {
                var conditions = new Hashtable();
                conditions.Add("personalID_codes", codes.ToArray());

                var data_taisyoku = OrdiaSqlMapper.GetInstance().QueryForList<ORjinjitaisyoku>("SelectORjinjitaisyokuBlock", conditions).ToList();
                result.AddRange(data_taisyoku);
            }

            return result;
        }

        /// <summary>
        /// ORDIA_個人給与情報からデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORkojinkyuuyo GetInfokojinkyuuyo(string personalID)
        {
            ORkojinkyuuyo param = new ORkojinkyuuyo();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkojinkyuuyo>("SelectORkojinkyuuyo", param);
        }

        /// <summary>
        /// ORDIA_個人給与情報から指定された個人IDを持つレコード群を抽出（EXCEL作成用）
        /// </summary>
        /// <param name="persona">個人IDリスト</param>
        /// <returns>検索結果</returns>
        public List<ORkojinkyuuyo> GetInfokojinkyuuyoBlock(List<string> persona)
        {
            var result = new List<ORkojinkyuuyo>();

            foreach (var codes in persona.Chunk(Constants.PostgresInClauseMaxCount))
            {
                var conditions = new Hashtable();
                conditions.Add("personalID_codes", codes.ToArray());

                var data_kojinkyuuyo = OrdiaSqlMapper.GetInstance().QueryForList<ORkojinkyuuyo>("SelectORkojinkyuuyoBlock", conditions).ToList();
                result.AddRange(data_kojinkyuuyo);
            }

            return result;
        }

        /// <summary>
        /// ORDIA_個人社会保険情報からデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORkojinsyakaihoken GetInfokojinsyakaihoken(string personalID)
        {
            ORkojinsyakaihoken param = new ORkojinsyakaihoken();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkojinsyakaihoken>("SelectORkojinsyakaihoken", param);
        }

        /// <summary>
        /// ORDIA_個人社会保険情報から指定された個人IDを持つレコード群を抽出（EXCEL作成用）
        /// </summary>
        /// <param name="persona">個人IDリスト</param>
        /// <returns>検索結果</returns>
        public List<ORkojinsyakaihoken> GetInfokojinsyakaihokenBlock(List<string> persona)
        {
            var result = new List<ORkojinsyakaihoken>();

            foreach (var codes in persona.Chunk(Constants.PostgresInClauseMaxCount))
            {
                var conditions = new Hashtable();
                conditions.Add("personalID_codes", codes.ToArray());

                var data_syakaihoken = OrdiaSqlMapper.GetInstance().QueryForList<ORkojinsyakaihoken>("SelectORkojinsyakaihokenBlock", conditions).ToList();
                result.AddRange(data_syakaihoken);
            }

            return result;
        }

        /// <summary>
        /// ORDIA_個人労働保険情報からデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORkojinroudouhoken GetInfokojinroudouhoken(string personalID)
        {
            ORkojinroudouhoken param = new ORkojinroudouhoken();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkojinroudouhoken>("SelectORkojinroudouhoken", param);
        }

        /// <summary>
        /// ORDIA_個人労働保険情報から現在保険加入している者のデータ取得（EXCEL作成用）
        /// </summary>
        /// <param name="end_date">抽出対象の雇用保険資格喪失日の始まり</param>
        /// <returns>検索結果</returns>
        public List<ORkojinroudouhoken> GetInfokojinroudouhokenCurrent(DateTime end_date)
        {
            ORkojinroudouhoken param = new ORkojinroudouhoken();
            param.employment_end_date = end_date;

            return OrdiaSqlMapper.GetInstance().QueryForList<ORkojinroudouhoken>("SelectORkojinroudouhokenCurrent", param).ToList();
        }

        /// <summary>
        /// ORDIA_健康保険改定データからデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORkaiteiDataKenpo GetInfokaiteiDataKenpo(string personalID)
        {
            ORkaiteiDataKenpo param = new ORkaiteiDataKenpo();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkaiteiDataKenpo>("SelectORkaiteiDataKenpo", param);
        }

        /// <summary>
        /// ORDIA_厚生年金改定データからデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public ORkaiteiDataKounen GetInfokaiteiDataKounen(string personalID)
        {
            ORkaiteiDataKounen param = new ORkaiteiDataKounen();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkaiteiDataKounen>("SelectORkaiteiDataKounen", param);
        }

        /// <summary>
        /// ORDIA_個人扶養者情報からデータ取得
        /// </summary>
        /// <param name="personalID">個人ID</param>
        /// <returns>検索結果</returns>
        public List<ORfuyou> GetInfofuyou(string personalID)
        {
            ORfuyou param = new ORfuyou();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForList<ORfuyou>("SelectORfuyou", param).ToList();
        }

        /// <summary>
        /// 所属マスタからデータ取得
        /// </summary>
        /// <param name="section_code">所属コード</param>
        /// <returns>検索結果</returns>
        public ORsyozoku GetInfosyozoku(string section_code)
        {
            ORsyozoku param = new ORsyozoku();
            param.section_code = section_code;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORsyozoku>("SelectORsyozoku", param);
        }

        /// <summary>
        /// 所属マスタからデータ取得（全件）
        /// </summary>
        /// <returns>検索結果</returns>
        public List<ORsyozoku> GetInfosyozokuAll()
        {
            return OrdiaSqlMapper.GetInstance().QueryForList<ORsyozoku>("SelectORsyozokuAll", null).ToList();
        }

        /// <summary>
        /// 給与計算パラメータからデータ取得
        /// </summary>
        /// <param name="section_code">所属コード</param>
        /// <returns>検索結果</returns>
        public ORkyuuyoKeisan GetInfoKyuuyoKeisan(string personalID)
        {
            ORkyuuyoKeisan param = new ORkyuuyoKeisan();
            param.personal_id = personalID;

            return OrdiaSqlMapper.GetInstance().QueryForObject<ORkyuuyoKeisan>("SelectORkyuuyoKeisan", param);

            // 処理年月が最新で、かつその処理年月内で処理回数が最大のレコードを取得する。
        }

        /// <summary>
        /// ORDIA_給与計算パラメータから指定された個人IDを持つレコード群を抽出（EXCEL作成用）
        /// </summary>
        /// <param name="persona">個人IDリスト</param>
        /// <returns>検索結果</returns>
        public List<ORkyuuyoKeisan> GetInfoKyuuyoKeisanBlock(List<string> persona)
        {
            var result = new List<ORkyuuyoKeisan>();

            foreach (var codes in persona.Chunk(Constants.PostgresInClauseMaxCount))
            {
                var conditions = new Hashtable();
                conditions.Add("personalID_codes", codes.ToArray());

                var data_KyuuyoKeisan = OrdiaSqlMapper.GetInstance().QueryForList<ORkyuuyoKeisan>("SelectORkyuuyoKeisanBlock", conditions).ToList();
                result.AddRange(data_KyuuyoKeisan);
            }

            return result;
        }

        /// <summary>
        /// 重複社員コード検索
        /// </summary>
        /// <param name="last_kana">カナ姓</param>
        /// <param name="first_kana">カナ名</param>
        /// <param name="birth_date">生年月日</param>
        /// <param name="employee_code">社員コード</param>
        /// <returns>検索結果</returns>
        public List<ORchoufuku> GetInfoChoufukuSyain(string last_kana, string first_kana, DateTime birth_date, string employee_code)
        {
            List<ORchoufuku> list_choufuku = new List<ORchoufuku>();
            ORchoufuku choufuku = new ORchoufuku();

            // 人事マスタより同姓同名をサーチ（カナで照合）
            List<ORjinji> list_jinji = new List<ORjinji>();

            ORjinji param1 = new ORjinji();
            param1.last_kana = last_kana;
            param1.first_kana = first_kana;

            list_jinji = OrdiaSqlMapper.GetInstance().QueryForList<ORjinji>("SelectORjinjiDouseiDoumei", param1).ToList();

            // 同姓同名者について給与人事マスタより生年月日が同じレコードをサーチ

            foreach (var jinji in list_jinji)
            {
                ORkyuuyojinji kjinji = new ORkyuuyojinji();
                ORkyuuyojinji param2 = new ORkyuuyojinji();
                param2.personal_id = jinji.personal_id;

                kjinji = OrdiaSqlMapper.GetInstance().QueryForObject<ORkyuuyojinji>("SelectORkyuuyojinji", param2);

                if (kjinji != null && kjinji.birth_date == birth_date && jinji.employee_code != employee_code)
                {
                    choufuku.employee_code = jinji.employee_code;
                    choufuku.last_kana = jinji.last_kana;
                    choufuku.first_kana = jinji.first_kana;
                    choufuku.birth_date = (DateTime)kjinji.birth_date;
                    list_choufuku.Add(choufuku);
                }
            }

            return list_choufuku;
        }
    }
}
