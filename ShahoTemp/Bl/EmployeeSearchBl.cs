﻿using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System.Collections.Generic;
using System.Linq;

namespace DensanKyuuyo.Bl
{
    public class EmployeeSearchBl
    {
        /// <summary>
        /// 検索条件に基づき社員情報を取得
        /// </summary>
        /// <param name="employeeCode">社員コード</param>
        /// <param name="name">社員名</param>
        /// <param name="nameKana">社員名カナ</param>
        /// <returns>検索結果</returns>
        public List<Employee> FetchEmployeeInfo(string employeeCode, string name, string nameKana)
        {
            Employee param = new Employee();
            param.employee_code = employeeCode;
            param.name = name;
            param.name_kana = nameKana;

            // 社員情報を取得
            return OrdiaMSqlMapper.GetInstance().QueryForList<Employee>("SelectEmployeeInfo", param).ToList();
        }

        /// <summary>
        /// 社員コードから社員名を取得
        /// </summary>
        /// <param name="employeeCode">社員コード</param>
        /// <returns>社員名</returns>
        public string GetEmployeeName(string employeeCode)
        {
            Employee ret = new Employee();

            // 社員情報を取得
            ret = OrdiaMSqlMapper.GetInstance().QueryForObject<Employee>("SelectEmployee", employeeCode);

            if (ret == null)
            {
                return string.Empty;
            }
            else
            {
                return ret.name;
            }
        }
    }
}
