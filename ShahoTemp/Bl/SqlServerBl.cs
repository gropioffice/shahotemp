﻿using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System.Collections.Generic;
using System.Linq;

namespace DensanKyuuyo.Bl
{
    public class SqlServerBl
    {
        /// <summary>
        /// 事業所マスタから全レコード取得
        /// </summary>
        /// <returns>検索結果</returns>
        public List<SqlServerJigyosyo> GetInfoJigyosyoALL()
        {
            return SqlServerSqlMapper.GetInstance().QueryForList<SqlServerJigyosyo>("SelectJigyosyoALL", null).ToList();
        }

        /// <summary>
        /// 事業所マスタから指定された事業所IDのレコードを取得
        /// </summary>
        /// <param name="jigyosyo_code">事業所コード</param>
        /// <returns>検索結果</returns>
        public SqlServerJigyosyo GetInfoJigyosyo(int jigyosyo_code)
        {
            var param = new SqlServerJigyosyo();
            param.雇用事業所ＩＤ = jigyosyo_code;

            return SqlServerSqlMapper.GetInstance().QueryForObject<SqlServerJigyosyo>("SelectJigyosyo", param);
        }
    }
}
