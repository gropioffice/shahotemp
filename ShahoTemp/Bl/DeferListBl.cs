﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ClosedXML.Excel;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;

namespace DensanKyuuyo.Bl
{
    public class DeferListBl
    {
        /// <summary>
        /// お知らせを検索する。
        /// </summary>
        /// <returns>お知らせリスト</returns>
        public List<DeferList> SearchNoticeForExcel()
        {
            // お知らせ情報の取得
            var list = GropShareSqlMapper.GetInstance().QueryForList<NoticeExcel>("SelectNoticeExcel", null).ToList();

            // メール送信対象名称セット・メール送信最終日時編集と氏名取得
            foreach (var w_list in list)
            {
                w_list.join_loss_class_char = w_list.join_loss_class == 1 ? "加入" : "喪失";

                DateTime? w_date = w_list.mail_send_datetime;
                w_list.mail_send_datetime_char = w_date == null ? string.Empty : w_date.Value.ToString("yyyy/MM/dd HH:mm:ss");
            }

            // 派遣社員マスタの取得
            string d_dummy = string.Empty;
            var dispatchWorkerRepository = new Common.DispatchWorkerRepository();
            var dlist = dispatchWorkerRepository.GetDispatchWorkers(list.Select(x => x.dispatch_worker_code).ToList());

            // 社員マスタの取得
            string e_dummy = string.Empty;
            var elist = OrdiaMSqlMapper.GetInstance().QueryForList<Employee>("SelectEmployeeAll", e_dummy).ToList();

            var query = list
                           .GroupJoin(
                                  dlist,
                                  l => l.dispatch_worker_code,
                                  d => d.dispatch_worker_code,
                                  (notice, dispatch) => new NoticeExcelTemp
                                  {
                                      dispatch_worker_code = notice.dispatch_worker_code,
                                      mail_send_datetime = notice.mail_send_datetime,
                                      manager_code = notice.manager_code,
                                      join_loss_class = notice.join_loss_class,
                                      defer_name_1 = notice.defer_name_1,
                                      defer_name_2 = notice.defer_name_2,
                                      defer_name_3 = notice.defer_name_3,
                                      defer_name_4 = notice.defer_name_4,
                                      defer_name_5 = notice.defer_name_5,
                                      remark_1 = notice.remark_1,
                                      remark_2 = notice.remark_2,
                                      join_loss_class_char = notice.join_loss_class_char,
                                      mail_send_datetime_char = notice.mail_send_datetime_char,
                                      dispatch_worker_name = dispatch.Any() ? dispatch.FirstOrDefault().name : string.Empty,
                                      department_name = dispatch.Any() ? dispatch.FirstOrDefault().department_name : string.Empty,
                                      health_insurance_join_date = dispatch.Any() ? dispatch.FirstOrDefault().health_insurance_join_date : null,
                                      health_insurance_loss_date = dispatch.Any() ? dispatch.FirstOrDefault().health_insurance_loss_date : null,
                                      employment_insurance_join_date = dispatch.Any() ? dispatch.FirstOrDefault().employment_insurance_join_date : null,
                                      employment_insurance_loss_date = dispatch.Any() ? dispatch.FirstOrDefault().employment_insurance_loss_date : null
                                  });

            var result = query
                           .GroupJoin(
                                  elist,
                                  q => q.manager_code,
                                  e => e.employee_code,
                                  (temp, employee) => new Model.DeferList
                                  {
                                      dispatch_worker_code = temp.dispatch_worker_code,
                                      mail_send_datetime = temp.mail_send_datetime,
                                      manager_code = temp.manager_code,
                                      join_loss_class = temp.join_loss_class,
                                      defer_name_1 = temp.defer_name_1,
                                      defer_name_2 = temp.defer_name_2,
                                      defer_name_3 = temp.defer_name_3,
                                      defer_name_4 = temp.defer_name_4,
                                      defer_name_5 = temp.defer_name_5,
                                      remark_1 = temp.remark_1,
                                      remark_2 = temp.remark_2,
                                      join_loss_class_char = temp.join_loss_class_char,
                                      mail_send_datetime_char = temp.mail_send_datetime_char,
                                      dispatch_worker_name = temp.dispatch_worker_name,
                                      department_name = temp.department_name,
                                      health_insurance_join_date = temp.health_insurance_join_date,
                                      health_insurance_loss_date = temp.health_insurance_loss_date,
                                      employment_insurance_join_date = temp.employment_insurance_join_date,
                                      employment_insurance_loss_date = temp.employment_insurance_loss_date,
                                      manager_name = employee.Any() ? employee.FirstOrDefault().name : string.Empty
                                  });

            var tempL = result.OrderBy(x => x.dispatch_worker_code);
            var sortedresult = tempL.OrderBy(x => x.department_name);

            return sortedresult.ToList();
        }

        /// <summary>
        /// 保留リストをEXCEL出力する。
        /// </summary>
        /// <param name="deferlist">保留リスト</param>
        /// <param name="filename">保存ファイル名</param>
        public void ExcelOutput(List<DeferList> deferlist, string filename)
        {
            DateTime? wk_join_date;
            DateTime? wk_loss_date;

            DataTable xls = new DataTable();
            xls.Columns.Add("社員コード");
            xls.Columns.Add("氏名");
            xls.Columns.Add("メール送信日");
            xls.Columns.Add("部門名");
            xls.Columns.Add("担当者コード");
            xls.Columns.Add("担当者名");
            xls.Columns.Add("加入喪失");
            xls.Columns.Add("取得日");
            xls.Columns.Add("退職日");
            xls.Columns.Add("保留内容１");
            xls.Columns.Add("保留内容２");
            xls.Columns.Add("保留内容３");
            xls.Columns.Add("保留内容４");
            xls.Columns.Add("保留内容５");
            xls.Columns.Add("備考１");
            xls.Columns.Add("備考２");

            foreach (var defer in deferlist)
            {
                // 健康保険資格取得日と雇用保険資格取得日を比較し、新しいほうを「取得日」とする。
                if (defer.health_insurance_join_date == null)
                {
                    wk_join_date = defer.employment_insurance_join_date;
                }
                else
                {
                    if (defer.employment_insurance_join_date == null)
                    {
                        wk_join_date = defer.health_insurance_join_date;
                    }
                    else
                    {
                        if (defer.health_insurance_join_date > defer.employment_insurance_join_date)
                        {
                            wk_join_date = defer.health_insurance_join_date;
                        }
                        else
                        {
                            wk_join_date = defer.employment_insurance_join_date;
                        }
                    }
                }

                // 健康保険資格喪失日と雇用保険資格喪失日を比較し、新しいほうを「退職日」とする。
                if (defer.health_insurance_loss_date == null)
                {
                    wk_loss_date = defer.employment_insurance_loss_date;
                }
                else
                {
                    if (defer.employment_insurance_loss_date == null)
                    {
                        wk_loss_date = defer.health_insurance_loss_date;
                    }
                    else
                    {
                        if (defer.health_insurance_loss_date > defer.employment_insurance_loss_date)
                        {
                            wk_loss_date = defer.health_insurance_loss_date;
                        }
                        else
                        {
                            wk_loss_date = defer.employment_insurance_loss_date;
                        }
                    }
                }

                DataRow xlsRow = xls.NewRow();
                xlsRow["社員コード"] = defer.dispatch_worker_code;
                xlsRow["氏名"] = defer.dispatch_worker_name;
                xlsRow["メール送信日"] = defer.mail_send_datetime_char;
                xlsRow["部門名"] = defer.department_name;
                xlsRow["担当者コード"] = defer.manager_code;
                xlsRow["担当者名"] = defer.manager_name;
                xlsRow["加入喪失"] = defer.join_loss_class_char;
                xlsRow["取得日"] = wk_join_date;
                xlsRow["退職日"] = wk_loss_date;
                xlsRow["保留内容１"] = defer.defer_name_1;
                xlsRow["保留内容２"] = defer.defer_name_2;
                xlsRow["保留内容３"] = defer.defer_name_3;
                xlsRow["保留内容４"] = defer.defer_name_4;
                xlsRow["保留内容５"] = defer.defer_name_5;
                xlsRow["備考１"] = defer.remark_1;
                xlsRow["備考２"] = defer.remark_2;

                xls.Rows.Add(xlsRow);
            }

            CreateExcelFile(xls, filename);
        }

        /// <summary>
        /// EXCEL形式のファイルを作成する。
        /// </summary>
        /// <param name="table">保留リストテーブル</param>
        /// <param name="fileName">保存ファイル名</param>
        private void CreateExcelFile(DataTable table, string fileName)
        {
            DateTime wksysdate = DateTime.Now;
            DateTime worningdate = new DateTime(wksysdate.Year, wksysdate.Month, wksysdate.Day, 23, 59, 59).AddMonths(-1);

            using (var wb = new XLWorkbook())
            {
                // ワークシートの設定
                IXLWorksheet ws = wb.AddWorksheet("保留リストExcel");

                int colIndex = 0;
                foreach (DataColumn col in table.Columns)
                {
                    ws.Cell(1, ++colIndex).Value = col.ColumnName;
                }

                int rowIndex = 2;
                foreach (DataRow row in table.Rows)
                {
                    colIndex = 0;
                    ws.Cell(rowIndex, ++colIndex).Value = row["社員コード"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["氏名"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["メール送信日"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["部門名"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["担当者コード"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["担当者名"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["加入喪失"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["取得日"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["退職日"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["保留内容１"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["保留内容２"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["保留内容３"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["保留内容４"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["保留内容５"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["備考１"].ToString();
                    ws.Cell(rowIndex, ++colIndex).Value = row["備考２"].ToString();

                    if (row["加入喪失"].ToString() == "加入")
                    {
                        if (!string.IsNullOrEmpty(row["取得日"].ToString()))
                        {
                            if (DateTime.Parse(row["取得日"].ToString()) <= worningdate)
                            {
                                ws.Range("A" + rowIndex.ToString() + ":" + "P" + rowIndex.ToString()).Style.Fill.BackgroundColor = XLColor.Red;
                            }
                        }
                    }

                    if (row["加入喪失"].ToString() == "喪失")
                    {
                        if (!string.IsNullOrEmpty(row["退職日"].ToString()))
                        {
                            if (DateTime.Parse(row["退職日"].ToString()) <= worningdate)
                            {
                                ws.Range("A" + rowIndex.ToString() + ":" + "P" + rowIndex.ToString()).Style.Fill.BackgroundColor = XLColor.Red;
                            }
                        }
                    }

                    rowIndex++;
                }

                SetExcelSheetFormat(ws);

                wb.SaveAs(fileName);
            }
        }

        /// <summary>
        /// 出力下Excelの書式設定をする。
        /// </summary>
        /// <param name="ws">シートオブジェクト</param>
        private void SetExcelSheetFormat(IXLWorksheet ws)
        {
            // フォント
            ws.Style.Font.FontName = "ＭＳ Ｐゴシック";

            // 文字表示位置 上
            ws.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;

            // 列幅自動調整
            ws.ColumnsUsed().AdjustToContents();

            // 罫線
            ws.Range(ws.Cell("A1").Address, ws.LastCellUsed().Address).Style
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin);

            // 項目名ヘッダー　色付け
            ws.Range(ws.Cell(1, 1), ws.Cell(1, ws.LastCellUsed().Address.ColumnNumber)).Style.Fill.BackgroundColor = XLColor.SkyBlue;
        }
    }
}