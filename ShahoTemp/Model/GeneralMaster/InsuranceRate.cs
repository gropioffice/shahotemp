﻿using GeneralMasterManagement.Data;
using System.Text.RegularExpressions;

namespace DensanKyuuyo.Model.GeneralMaster
{
    public class InsuranceRate : GeneralCategory
    {
        public InsuranceRate()
        {
            general_category_code = "3";
            general_category_name = "保険料率";

            general_string_column_1_name = "料率";
            var stringColumn1Validation = new ValidationSet<string>(x => !string.IsNullOrEmpty(x) && Regex.IsMatch(x, "^0.[0-9]{1,5}$"), "料率は1未満(小数点以下5桁まで)の数値を入力してください。");
            ValidationsOfGeneralString1.Add(stringColumn1Validation);
        }
    }
}