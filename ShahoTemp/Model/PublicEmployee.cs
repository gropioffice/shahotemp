﻿// <auto-generated />

using System;

namespace DensanKyuuyo.Model
{
    public class PublicEmployee
    {
        public string employee_no { get; set; }

        public string name { get; set; }

        public string name_kana { get; set; }

        public string company_code { get; set; }

        public string department_code { get; set; }

        public string position_code { get; set; }

        public string password_digest { get; set; }

        public string mobile_phone { get; set; }

        public string mail_address { get; set; }

        public string mobile_mail_address { get; set; }

        public string remarks { get; set; }

        public string retirement_flag { get; set; }

        public DateTime? regist_datetime { get; set; }

        public string regist_id { get; set; }

        public DateTime? update_datetime { get; set; }

        public string update_id { get; set; }

        // 以下、追加分
        public string system_name { get; set; }

    }
}
