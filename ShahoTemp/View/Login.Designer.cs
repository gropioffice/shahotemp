﻿namespace DensanKyuuyo.View
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btnログイン = new GropCommonLib.Controls.GButton();
            this.Btn終了 = new GropCommonLib.Controls.GButton();
            this.Txt社員番号 = new GropCommonLib.Controls.GTextBox();
            this.Txtパスワード = new GropCommonLib.Controls.GTextBox();
            this.Lbl社員番号 = new GropCommonLib.Controls.GLabel();
            this.Lblパスワード = new GropCommonLib.Controls.GLabel();
            this.panel1 = new GropCommonLib.Controls.GPanel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btnログイン
            // 
            this.Btnログイン.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(232)))), ((int)(((byte)(202)))));
            this.Btnログイン.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.Btnログイン.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Btnログイン.GButtonType = GropCommonLib.Controls.GButton.ButtonType.ScreenTransition;
            this.Btnログイン.Location = new System.Drawing.Point(50, 111);
            this.Btnログイン.Name = "Btnログイン";
            this.Btnログイン.Size = new System.Drawing.Size(104, 55);
            this.Btnログイン.TabIndex = 4;
            this.Btnログイン.Text = "ログイン";
            this.Btnログイン.UseVisualStyleBackColor = false;
            this.Btnログイン.Click += new System.EventHandler(this.Btnログイン_Click);
            // 
            // Btn終了
            // 
            this.Btn終了.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Btn終了.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.Btn終了.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Btn終了.GButtonType = GropCommonLib.Controls.GButton.ButtonType.Close;
            this.Btn終了.Location = new System.Drawing.Point(224, 111);
            this.Btn終了.Name = "Btn終了";
            this.Btn終了.Size = new System.Drawing.Size(104, 55);
            this.Btn終了.TabIndex = 5;
            this.Btn終了.Text = "終了";
            this.Btn終了.UseVisualStyleBackColor = false;
            this.Btn終了.Click += new System.EventHandler(this.Btn終了_Click);
            // 
            // Txt社員番号
            // 
            this.Txt社員番号.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Txt社員番号.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Txt社員番号.GInputFormat = GropCommonLib.Controls.GTextBox.GFormatType.Integer;
            this.Txt社員番号.GIsChecked = true;
            this.Txt社員番号.GItemName = "社員番号";
            this.Txt社員番号.GMaxValue = -1;
            this.Txt社員番号.GMinimumValue = -1;
            this.Txt社員番号.GRequired = true;
            this.Txt社員番号.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Txt社員番号.Location = new System.Drawing.Point(155, 30);
            this.Txt社員番号.MaxLength = 6;
            this.Txt社員番号.Name = "Txt社員番号";
            this.Txt社員番号.Size = new System.Drawing.Size(130, 23);
            this.Txt社員番号.TabIndex = 1;
            // 
            // Txtパスワード
            // 
            this.Txtパスワード.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Txtパスワード.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Txtパスワード.GInputFormat = GropCommonLib.Controls.GTextBox.GFormatType.AlphaNumericSymbol;
            this.Txtパスワード.GIsChecked = true;
            this.Txtパスワード.GItemName = "パスワード";
            this.Txtパスワード.GMaxValue = -1;
            this.Txtパスワード.GMinimumValue = -1;
            this.Txtパスワード.GRequired = true;
            this.Txtパスワード.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Txtパスワード.Location = new System.Drawing.Point(155, 62);
            this.Txtパスワード.MaxLength = 20;
            this.Txtパスワード.Name = "Txtパスワード";
            this.Txtパスワード.PasswordChar = '*';
            this.Txtパスワード.Size = new System.Drawing.Size(130, 23);
            this.Txtパスワード.TabIndex = 3;
            // 
            // Lbl社員番号
            // 
            this.Lbl社員番号.AutoSize = true;
            this.Lbl社員番号.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Lbl社員番号.Location = new System.Drawing.Point(77, 33);
            this.Lbl社員番号.Name = "Lbl社員番号";
            this.Lbl社員番号.Size = new System.Drawing.Size(72, 16);
            this.Lbl社員番号.TabIndex = 0;
            this.Lbl社員番号.Text = "社員番号";
            // 
            // Lblパスワード
            // 
            this.Lblパスワード.AutoSize = true;
            this.Lblパスワード.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Lblパスワード.Location = new System.Drawing.Point(79, 65);
            this.Lblパスワード.Name = "Lblパスワード";
            this.Lblパスワード.Size = new System.Drawing.Size(70, 16);
            this.Lblパスワード.TabIndex = 2;
            this.Lblパスワード.Text = "パスワード";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(202)))), ((int)(((byte)(118)))));
            this.panel1.Controls.Add(this.Lblパスワード);
            this.panel1.Controls.Add(this.Lbl社員番号);
            this.panel1.Controls.Add(this.Txtパスワード);
            this.panel1.Controls.Add(this.Txt社員番号);
            this.panel1.Controls.Add(this.Btn終了);
            this.panel1.Controls.Add(this.Btnログイン);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(382, 186);
            this.panel1.TabIndex = 0;
            // 
            // Login
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(233)))), ((int)(((byte)(202)))));
            this.ClientSize = new System.Drawing.Size(406, 210);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ログイン";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GropCommonLib.Controls.GButton Btnログイン;
        private GropCommonLib.Controls.GButton Btn終了;
        private GropCommonLib.Controls.GTextBox Txt社員番号;
        private GropCommonLib.Controls.GTextBox Txtパスワード;
        private GropCommonLib.Controls.GLabel Lbl社員番号;
        private GropCommonLib.Controls.GLabel Lblパスワード;
        private GropCommonLib.Controls.GPanel panel1;
    }
}