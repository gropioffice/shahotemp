﻿namespace DensanKyuuyo.View
{
    partial class ProgressDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Pgb送信状況 = new System.Windows.Forms.ProgressBar();
            this.Lbl件数 = new GropCommonLib.Controls.GLabel();
            this.BtnOK = new GropCommonLib.Controls.GButton();
            this.SuspendLayout();
            // 
            // Pgb送信状況
            // 
            this.Pgb送信状況.Location = new System.Drawing.Point(12, 40);
            this.Pgb送信状況.Name = "Pgb送信状況";
            this.Pgb送信状況.Size = new System.Drawing.Size(348, 23);
            this.Pgb送信状況.TabIndex = 0;
            // 
            // Lbl件数
            // 
            this.Lbl件数.AutoSize = true;
            this.Lbl件数.Font = new System.Drawing.Font("MS UI Gothic", 11F);
            this.Lbl件数.Location = new System.Drawing.Point(9, 22);
            this.Lbl件数.Name = "Lbl件数";
            this.Lbl件数.Size = new System.Drawing.Size(0, 15);
            this.Lbl件数.TabIndex = 1;
            // 
            // BtnOK
            // 
            this.BtnOK.BackColor = System.Drawing.SystemColors.Control;
            this.BtnOK.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.BtnOK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnOK.GButtonType = GropCommonLib.Controls.GButton.ButtonType.Default;
            this.BtnOK.Location = new System.Drawing.Point(151, 69);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(69, 36);
            this.BtnOK.TabIndex = 2;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = false;
            this.BtnOK.Visible = false;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // ProgressDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 117);
            this.ControlBox = false;
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.Lbl件数);
            this.Controls.Add(this.Pgb送信状況);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "送信状況";
            this.Shown += new System.EventHandler(this.ProgressDialog_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar Pgb送信状況;
        private GropCommonLib.Controls.GLabel Lbl件数;
        private GropCommonLib.Controls.GButton BtnOK;
    }
}