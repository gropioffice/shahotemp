﻿using GropCommonLib;
using DensanKyuuyo.Bl;
using DensanKyuuyo.Common;
using DensanKyuuyo.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace SocialInsuranceSystem.View
{
    public partial class EshiharaiD : Form
    {
        private static EshiharaiD instance;

        /// <summary>
        /// Singleton パターン
        /// </summary>
        /// <returns>インスタンス</returns>
        public static EshiharaiD GetInstance()
        {
            if (instance == null || instance.IsDisposed)
            {
                instance = new EshiharaiD();
            }

            return instance;
        }

        public EshiharaiD()
        {
            InitializeComponent();
        }

        /// <summary>
        /// いいえボタンクリック時処理
        /// </summary>
        /// <param name="sender">イベント発生元オブジェクト</param>
        /// <param name="e">イベント補足情報</param>
        private void BtnNo_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// はいボタンクリック時処理
        /// </summary>
        /// <param name="sender">イベント発生元オブジェクト</param>
        /// <param name="e">イベント補足情報</param>
        private void BtnYes_Click(object sender, EventArgs e)
        {
            if (GMessageUtil.Question("更新処理を行います。よろしいですか？") == DialogResult.No)
            {
                return;
            }

            EshiharaiDBl bl = new EshiharaiDBl();

            if (bl.CreateEshiharaiD())
            {
                GMessageUtil.Information("更新処理を終了しました。");
                Close();
            }

        }

        private void EshiharaiD_Load(object sender, EventArgs e)
        {
            EshiharaiDBl bl = new EshiharaiDBl();
            bl.GetSyoriKikan();

            LblStartDate.Text = bl.DateStart.ToString("yyyy/MM/dd");
            LblEndDate.Text = bl.DateEnd.ToString("yyyy/MM/dd");
        }
    }
}