﻿namespace DensanKyuuyo.View
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripメニュー = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.TsmE支払D更新 = new System.Windows.Forms.ToolStripMenuItem();
            this.TsmE賃金D更新 = new System.Windows.Forms.ToolStripMenuItem();
            this.tbtn終了 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnマスタ管理 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Tslログイン名 = new System.Windows.Forms.ToolStripLabel();
            this.tss仕切り線1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripメニュー.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripメニュー
            // 
            this.toolStripメニュー.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.toolStripメニュー.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStripメニュー.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.tbtn終了,
            this.toolStripSeparator4,
            this.tbtnマスタ管理,
            this.toolStripSeparator1,
            this.Tslログイン名});
            this.toolStripメニュー.Location = new System.Drawing.Point(0, 0);
            this.toolStripメニュー.Name = "toolStripメニュー";
            this.toolStripメニュー.Size = new System.Drawing.Size(1184, 55);
            this.toolStripメニュー.TabIndex = 0;
            this.toolStripメニュー.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TsmE支払D更新,
            this.TsmE賃金D更新});
            this.toolStripDropDownButton1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.toolStripDropDownButton1.Image = global::DensanKyuuyo.Properties.Resources.file_write;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(85, 52);
            this.toolStripDropDownButton1.Text = "月次処理";
            this.toolStripDropDownButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // TsmE支払D更新
            // 
            this.TsmE支払D更新.Image = global::DensanKyuuyo.Properties.Resources.file_write;
            this.TsmE支払D更新.Name = "TsmE支払D更新";
            this.TsmE支払D更新.Size = new System.Drawing.Size(175, 38);
            this.TsmE支払D更新.Text = "E支払D更新";
            this.TsmE支払D更新.Click += new System.EventHandler(this.TsmE支払D更新_Click);
            // 
            // TsmE賃金D更新
            // 
            this.TsmE賃金D更新.Image = global::DensanKyuuyo.Properties.Resources.file_write;
            this.TsmE賃金D更新.Name = "TsmE賃金D更新";
            this.TsmE賃金D更新.Size = new System.Drawing.Size(175, 38);
            this.TsmE賃金D更新.Text = "E賃金D更新";
            this.TsmE賃金D更新.Click += new System.EventHandler(this.TsmE賃金D更新_Click);
            // 
            // tbtn終了
            // 
            this.tbtn終了.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtn終了.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbtn終了.ForeColor = System.Drawing.Color.Red;
            this.tbtn終了.Image = global::DensanKyuuyo.Properties.Resources.終了;
            this.tbtn終了.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtn終了.Name = "tbtn終了";
            this.tbtn終了.Size = new System.Drawing.Size(70, 52);
            this.tbtn終了.Text = "　終了　";
            this.tbtn終了.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbtn終了.ToolTipText = "終了";
            this.tbtn終了.Click += new System.EventHandler(this.Tbtn終了_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 55);
            // 
            // tbtnマスタ管理
            // 
            this.tbtnマスタ管理.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnマスタ管理.Enabled = false;
            this.tbtnマスタ管理.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tbtnマスタ管理.ForeColor = System.Drawing.Color.Black;
            this.tbtnマスタ管理.Image = global::DensanKyuuyo.Properties.Resources.gear;
            this.tbtnマスタ管理.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnマスタ管理.Name = "tbtnマスタ管理";
            this.tbtnマスタ管理.Size = new System.Drawing.Size(79, 52);
            this.tbtnマスタ管理.Text = "マスタ管理";
            this.tbtnマスタ管理.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbtnマスタ管理.ToolTipText = "終了";
            this.tbtnマスタ管理.Click += new System.EventHandler(this.Tbtnマスタ管理_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 55);
            // 
            // Tslログイン名
            // 
            this.Tslログイン名.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Tslログイン名.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Tslログイン名.Name = "Tslログイン名";
            this.Tslログイン名.Size = new System.Drawing.Size(192, 52);
            this.Tslログイン名.Text = "ログイン名：グロップ　太郎さん";
            // 
            // tss仕切り線1
            // 
            this.tss仕切り線1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tss仕切り線1.Name = "tss仕切り線1";
            this.tss仕切り線1.Size = new System.Drawing.Size(6, 55);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1184, 749);
            this.Controls.Add(this.toolStripメニュー);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "社保システム";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStripメニュー.ResumeLayout(false);
            this.toolStripメニュー.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStripメニュー;
        private System.Windows.Forms.ToolStripButton tbtn終了;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel Tslログイン名;
        private System.Windows.Forms.ToolStripSeparator tss仕切り線1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbtnマスタ管理;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem TsmE賃金D更新;
        private System.Windows.Forms.ToolStripMenuItem TsmE支払D更新;
    }
}