﻿using GropCommonLib;
using DensanKyuuyo.Bl;
using System;
using System.Windows.Forms;

namespace DensanKyuuyo.View
{
    public partial class Login : Form
    {
        private static Login instance;

        /// <summary>
        /// Singleton パターン
        /// </summary>
        /// <returns>インスタンス</returns>
        public static Login GetInstance()
        {
            if (instance == null || instance.IsDisposed)
            {
                instance = new Login();
            }

            return instance;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Login()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ログインボタンクリックイベント
        /// ユーザID、パスワードを照合してログイン処理を行う。
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">イベントハンドラ</param>
        private void Btnログイン_Click(object sender, EventArgs e)
        {
            // 入力チェック
            if (!InputCheck())
            {
                return;
            }

            LoginBl bl = new LoginBl();
            bool res = bl.GetEmployeeInfos(Txt社員番号.Text, Txtパスワード.Text);
            if (!res)
            {
                GMessageUtil.Error("社員番号、またはパスワードが違います。");

                Txt社員番号.Text = string.Empty;
                Txtパスワード.Text = string.Empty;

                Txt社員番号.Focus();

                return;
            }

            MainForm frm = (MainForm)this.MdiParent;
            frm.SetLoginName(Program.loginInfo.name);
            frm.SetToolStripMenuVisible(true);
            this.Close();
        }

        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <returns>チェック結果</returns>
        private bool InputCheck()
        {
            if (!Txt社員番号.InputCheck())
            {
                return false;
            }

            if (!Txtパスワード.InputCheck())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 終了ボタンクリックイベント
        /// システムを終了する。
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">イベントハンドラ</param>
        private void Btn終了_Click(object sender, EventArgs e)
        {
            this.Close();
            Program.objMain.Close();
        }
    }
}