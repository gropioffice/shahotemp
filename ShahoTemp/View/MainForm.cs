﻿using GeneralMasterManagement.Data;
using GeneralMasterManagement.Data.Source.Repository.Ibatis;
using GeneralMasterManagement.View;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Document;
using GropCommonLib;
using DensanKyuuyo.Bl;
using DensanKyuuyo.Model;
using DensanKyuuyo.Model.GeneralMaster;
using DensanKyuuyo.SqlMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using SocialInsuranceSystem.View;

namespace DensanKyuuyo.View
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            // メニューを非表示にする
            SetToolStripMenuVisible(false);

            SetFormTitle();
        }

        /// <summary>
        /// メニューを表示/非表示する。
        /// </summary>
        /// <param name="view">表示/非表示フラグ</param>
        public void SetToolStripMenuVisible(bool view)
        {
            toolStripメニュー.Visible = view;
        }

        /// <summary>
        /// ログイン名を表示する。
        /// </summary>
        /// <param name="loginName">ログイン名</param>
        public void SetLoginName(string loginName)
        {
            this.Tslログイン名.Text = "ログイン名：" + loginName + "さん";
        }

        /// <summary>
        /// ビルド構成に応じたフォーム名を設定する
        /// </summary>
        private void SetFormTitle()
        {
            SetFormTitleDevelop();
            SetFormTitleGrop();
            SetFormTitleJoy();
        }

        /// <summary>
        /// フォームタイトルの設定(開発用)
        /// </summary>
        [Conditional("DEBUG")]
        private void SetFormTitleDevelop()
        {
            this.Text = "給与管理システム";
        }

        /// <summary>
        /// フォームタイトルの設定(GROP用)
        /// </summary>
        [Conditional("RELEASE_GROP")]
        private void SetFormTitleGrop()
        {
            this.Text = "給与管理システム(GROP)";
        }

        /// <summary>
        /// フォームタイトルの設定(JOY用)
        /// </summary>
        [Conditional("RELEASE_JOY")]
        private void SetFormTitleJoy()
        {
            this.Text = "給与管理システム(JOY)";
        }

        /// <summary>
        /// Formロードイベント
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">イベントハンドラ</param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            Login login = Login.GetInstance();
            login.MdiParent = this;
            login.Show();
        }

        /// <summary>
        /// 終了ボタン押下
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">イベントハンドラ</param>
        private void Tbtn終了_Click(object sender, EventArgs e)
        {
            DialogResult res = GMessageUtil.Question("終了します。よろしいですか？");
            if (res == DialogResult.No)
            {
                return;
            }

            this.Close();
        }

        /// <summary>
        /// マスタ管理ボタン押下
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">イベントハンドラ</param>
        private void Tbtnマスタ管理_Click(object sender, EventArgs e)
        {
            OpenGeneralMasterManagementForm();
        }

        /// <summary>
        /// 汎用マスタ管理画面を開く
        /// </summary>
        private void OpenGeneralMasterManagementForm()
        {
            var categories = new List<GeneralCategory>();
            categories.Add(new InsuranceRate());

            var repository = new GeneralItemRepository(GropShareSqlMapper.GetInstance(), Program.loginInfo.employee_no);
            var form = new GeneralItemManagementForm(categories, repository);

            form.ShowDialog();
        }

        /// <summary>
        /// E支払D更新メニュークリック時処理
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">イベントハンドラ</param>
        private void TsmE支払D更新_Click(object sender, EventArgs e)
        {
            EshiharaiD shiharai = EshiharaiD.GetInstance();
            shiharai.Show();
        }

        /// <summary>
        /// E賃金D更新メニュークリック時処理
        /// </summary>
        /// <param name="sender">オブジェクト</param>
        /// <param name="e">イベントハンドラ</param>
        private void TsmE賃金D更新_Click(object sender, EventArgs e)
        {

        }
    }
}