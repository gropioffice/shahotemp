﻿using GropCommonLib;
using DensanKyuuyo.Bl;
using DensanKyuuyo.Common;
using DensanKyuuyo.Model;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DensanKyuuyo.View
{
    public partial class ProgressDialog : Form, INoticeSendProgressCallbak
    {
        private List<Notice> targetlist;

        private string rcvMessage;
        private int totalCounter = 0;
        private int sentCounter = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="targets">送信対象お知らせリスト</param>
        public ProgressDialog(List<Notice> targets)
        {
            InitializeComponent();

            targetlist = targets;
        }

        /// <summary>
        /// 処理状況メッセージの受信
        /// </summary>
        /// <param name="message">処理状況メッセージ</param>
        public void SetStatusMessage(string message)
        {
            rcvMessage = message;
            ProgressDisplay();
        }

        /// <summary>
        /// 総件数の受信
        /// </summary>
        /// <param name="totalNumber">総件数</param>
        public void SetTotalNumber(int totalNumber)
        {
            totalCounter = totalNumber;
            ProgressDisplay();
        }

        /// <summary>
        /// 送信済件数の受信
        /// </summary>
        /// <param name="numberOfSent">送信済件数</param>
        public void SetNumberOfSent(int numberOfSent)
        {
            sentCounter = numberOfSent;
            ProgressDisplay();
        }

        /// <summary>
        /// 完了メッセージの受信
        /// </summary>
        /// <param name="totalNumberOfSent">総送信件数</param>
        /// <param name="numberOfSentError">送信失敗件数</param>
        public void OnSendFinished(int totalNumberOfSent, int numberOfSentError)
        {
            BtnOK.Visible = true;
        }

        /// <summary>
        /// メッセージとプログレスバーの表示
        /// </summary>
        private void ProgressDisplay()
        {
            Pgb送信状況.Maximum = totalCounter;
            Pgb送信状況.Value = sentCounter;
            Application.DoEvents();

            Lbl件数.Text = rcvMessage + "（" + sentCounter.ToString() + " / " + totalCounter.ToString() + "）";
            Application.DoEvents();
        }

        /// <summary>
        /// 初期処理
        /// </summary>
        private void InitialProcess()
        {
            Pgb送信状況.Minimum = 0;
            Pgb送信状況.Maximum = 0;
            Pgb送信状況.Value = 0;
            Lbl件数.Text = string.Empty;

            // メール送信への引数リストを作成
            var bl = new ProgressDialogBl();
            List<Employee> employees = bl.GetAllEmployee();
            List<DispatchWorker> dispatchWorkers = bl.GetAllDispatchWorker(targetlist);
            List<DeferReasonMaster> deferReasons = bl.GetAllDeferReasonMaster();

            // メール送信を呼び出す
            NoticeSender noticesender = new NoticeSender(employees, dispatchWorkers, deferReasons);
            noticesender.Send(targetlist, this);

            return;
        }

        /// <summary>
        /// ＯＫボタンクリック処理
        /// </summary>
        /// <param name="sender">イベント発生元オブジェクト</param>
        /// <param name="e">イベント補足情報</param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// フォーム表示後処理
        /// </summary>
        /// <param name="sender">イベント発生元オブジェクト</param>
        /// <param name="e">イベント補足情報</param>
        private void ProgressDialog_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();

            try
            {
                InitialProcess();
            }
            catch (Exception ee)
            {
                BtnOK.Visible = true;
                GMessageUtil.Halt(ee);
            }
        }
    }
}
