﻿// <auto-generated />
using DensanKyuuyo.Common;
using DensanKyuuyo.View;
using System;
using System.Windows.Forms;

namespace DensanKyuuyo
{
    internal static class Program
    {
        public static MainForm objMain;

        public static LoginInfo loginInfo;

        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです
        /// </summary>
        [STAThread]
        private static void Main()
        {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                loginInfo = new LoginInfo();
                objMain = new MainForm();
                Application.Run(objMain);
           
        }
    }
}