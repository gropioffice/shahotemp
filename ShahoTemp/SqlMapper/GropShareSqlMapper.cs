﻿using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using System.Diagnostics;

namespace DensanKyuuyo.SqlMapper
{
    public static class GropShareSqlMapper
    {
        private static ISqlMapper instance;

        /// <summary>
        /// 社保システム用SQLマッパー
        /// </summary>
        /// <returns>マッパーインスタンス</returns>
        public static ISqlMapper GetInstance()
        {
            if (instance == null)
            {
                CreateInstance();
            }

            return instance;
        }

        private static void CreateInstance()
        {
            CreateDevelopmentInstance();
            CreateGropInstance();
            CreateJoyInstance();
        }

        [Conditional("DEBUG")]
        private static void CreateDevelopmentInstance()
        {
            DomSqlMapBuilder builder = new DomSqlMapBuilder();

            instance = builder.Configure(@"Config\Connections\SqlMap\Develop\SqlMap.config");
        }

        [Conditional("RELEASE_GROP")]
        private static void CreateGropInstance()
        {
            DomSqlMapBuilder builder = new DomSqlMapBuilder();
            instance = builder.Configure(@"Config\Connections\SqlMap\ReleaseGrop\SqlMap.config");
        }

        [Conditional("RELEASE_JOY")]
        private static void CreateJoyInstance()
        {
            DomSqlMapBuilder builder = new DomSqlMapBuilder();
            instance = builder.Configure(@"Config\Connections\SqlMap\ReleaseJoy\SqlMap.config");
        }
    }
}