﻿using GropCommonLib;
using DensanKyuuyo.Bl;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DensanKyuuyo.Common
{
    public class DispatchWorkerRepository
    {
        /// <summary>
        /// 派遣社員情報を全件取得
        /// </summary>
        /// <returns>派遣社員情報</returns>
        public List<DispatchWorker> GetDispatchWorkers()
        {
            var temp = OrdiaSqlMapper.GetInstance().QueryForList<DispatchWorker>("SelectAllDispatchWorkers", null).ToList();

            var bl = new DispatchWorkerForOrdiaBl();
            return bl.GetStaffList(temp);
        }

        /// <summary>
        /// 指定された派遣社員情報を取得
        /// </summary>
        /// <param name="dispatchWorkerCodes">派遣社員コードリスト</param>
        /// <returns>派遣社員情報</returns>
        public List<DispatchWorker> GetDispatchWorkers(List<string> dispatchWorkerCodes)
        {
            var result = new List<DispatchWorker>();

            foreach (var codes in dispatchWorkerCodes.Chunk(Constants.PostgresInClauseMaxCount))
            {
                var conditions = new Hashtable();
                conditions.Add("dispatch_worker_codes", codes.ToArray());

                var dispatchWorkers = OrdiaSqlMapper.GetInstance().QueryForList<DispatchWorker>("SelectDispatchWorkers", conditions).ToList();
                result.AddRange(dispatchWorkers);
            }

            var bl = new DispatchWorkerForOrdiaBl();
            return bl.GetStaffList(result);
        }

        /// <summary>
        /// 指定された派遣社員情報を取得
        /// </summary>
        /// <param name="dispatchWorkerCode">派遣社員コード</param>
        /// <returns>派遣社員情報</returns>
        public DispatchWorker GetDispatchWorker(string dispatchWorkerCode)
        {
            var temp = OrdiaSqlMapper.GetInstance().QueryForObject<DispatchWorker>("SelectDispatchWorker", dispatchWorkerCode);

            var bl = new DispatchWorkerForOrdiaBl();
            return bl.GetStaffInfo(temp);
        }
    }
}