﻿namespace DensanKyuuyo.Common
{
    /// <summary>
    /// お知らせ送信進捗コールバック
    /// </summary>
    public interface INoticeSendProgressCallbak
    {
        /// <summary>
        /// 処理状況メッセージの設定
        /// </summary>
        /// <param name="message">処理状況メッセージ</param>
        void SetStatusMessage(string message);

        /// <summary>
        /// 総件数の設定
        /// </summary>
        /// <param name="totalNumber">総件数</param>
        void SetTotalNumber(int totalNumber);

        /// <summary>
        /// 送信済件数の設定
        /// </summary>
        /// <param name="numberOfSent">送信済件数</param>
        void SetNumberOfSent(int numberOfSent);

        /// <summary>
        /// 送信処理終了時コールバック
        /// </summary>
        /// <param name="totalNumberOfSent">総送信件数（失敗含む）</param>
        /// <param name="numberOfSentError">送信失敗件数</param>
        void OnSendFinished(int totalNumberOfSent, int numberOfSentError);
    }
}