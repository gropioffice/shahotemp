﻿namespace DensanKyuuyo.Common
{
    public static class Constants
    {
        // 汎用マスタカテゴリコード
        public const string GeneralCategoryCodeStatus = "1";

        public const string GeneralCategoryCodeJoinLossClass = "2";

        // 加入喪失区分
        public const int ClassJoin = 1;

        public const int ClassLoss = 2;

        // 状況
        public const int StatusOK = 1;

        public const int StatusNG = 2;

        public const string FlagOff = "0";
        public const string FlagOn = "1";

        /// <summary>
        /// データ件数標示フォーマット
        /// </summary>
        public const string DataNumberFormat = "件数:  {0}件";

        public const string SystemName = "social_insurance_system";

        public const int MailSendResultMaxLength = 50;

        public const int PostgresInClauseMaxCount = 2000;

        // 検索結果グリッドの列順（必要なもののみ記載）
        public const int GridMailSendFlag = 4;
    }
}