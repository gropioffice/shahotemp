﻿using GropCommonLib;
using DensanKyuuyo.Model;
using DensanKyuuyo.SqlMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DensanKyuuyo.Common
{
    public class NoticeSender
    {
        private readonly string smtpServer = "sendonly.grop.co.jp";
        private readonly int port = 587;

#if DEBUG
        private readonly string user = "sysdev_ml@grop.co.jp";
        private readonly string password = "Huj7GFnVG4";
        private readonly string from = "sysdev_ml@grop.co.jp";
        private readonly string fromName = "sysdev_ml@grop.co.jp";
        private readonly string cc = "sysdev_ml@grop.co.jp";
        private readonly string ccName = "sysdev_ml@grop.co.jp";
#else
        private readonly string user = "shaho@grop.co.jp";
        private readonly string password = "MrN3ibte";
        private readonly string from = "shaho@grop.co.jp";
        private readonly string fromName = "shaho@grop.co.jp";
        private readonly string cc = "shaho@grop.co.jp";
        private readonly string ccName = "shaho@grop.co.jp";
#endif

        private readonly string signature;
        private readonly int sendInterval = 2000;

        private List<Employee> employees;
        private List<DispatchWorker> dispatchWorkers;
        private List<DeferReasonMaster> deferReasons;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="employees">社員リスト</param>
        /// <param name="dispatchWorkers">派遣社員リスト</param>
        /// <param name="deferReasons">保留理由マスタ</param>
        /// <remarks>各種引数を利用してメールを作成する</remarks>
        public NoticeSender(List<Employee> employees, List<DispatchWorker> dispatchWorkers, List<DeferReasonMaster> deferReasons)
        {
            this.employees = employees;
            this.dispatchWorkers = dispatchWorkers;
            this.deferReasons = deferReasons;

            var signature = new StringBuilder();
            signature.AppendLine("*******************************");
            signature.AppendLine("株式会社 グロップ");
            signature.AppendLine("GS本部 総務部 社会保険チーム");
            signature.AppendLine("086-897-3660、FAX086-270-7762");
            signature.AppendLine("mailto: shaho@grop.co.jp");
            signature.AppendLine("*******************************");
            this.signature = signature.ToString();
        }

        /// <summary>
        /// 送信処理
        /// </summary>
        /// <param name="sendTargets">送信対象お知らせリスト</param>
        /// <param name="callback">プログレスコールバック</param>
        public async void Send(List<Notice> sendTargets, INoticeSendProgressCallbak callback = null)
        {
            FillManagerCode(sendTargets);

            var mailSender = new MailSender(smtpServer, port, false, user, password);
            var groupedNotice = sendTargets.GroupBy(x => new { x.manager_code, x.status });
            var totalNumber = groupedNotice.Count();
            var sentNumber = 0;
            var errorNumber = 0;

            callback?.SetStatusMessage("メール送信中...");
            callback?.SetTotalNumber(totalNumber);
            callback?.SetNumberOfSent(sentNumber);

            foreach (var noticesOnSameMail in groupedNotice)
            {
                var manager = employees.Where(x => x.employee_code == noticesOnSameMail.FirstOrDefault().manager_code).FirstOrDefault();

                if (manager == null)
                {
                    UpdateNoticeOnFailed(noticesOnSameMail.ToList(), "担当者未設定のためメール送信不可");
                    callback?.SetNumberOfSent(++sentNumber);
                    errorNumber++;
                    continue;
                }

                if (string.IsNullOrEmpty(manager.mail_address))
                {
                    UpdateNoticeOnFailed(noticesOnSameMail.ToList(), "担当者メールアドレス未設定のためメール送信不可");
                    callback?.SetNumberOfSent(++sentNumber);
                    errorNumber++;
                    continue;
                }

                var to = manager.mail_address;
                var toName = manager.mail_address;
                var subject = MakeSubject(noticesOnSameMail.FirstOrDefault().status);
                var body = MakeBody(manager.name, noticesOnSameMail.ToList());

                try
                {
                    Thread.Sleep(sendInterval);
                    await mailSender.SendAsync(from, fromName, to, toName, cc, ccName, subject, body);
                    UpdateNoticeOnSuccess(noticesOnSameMail.ToList());
                }
                catch (Exception ex)
                {
                    UpdateNoticeOnFailed(noticesOnSameMail.ToList(), $"送信失敗 {ex.ToString()}");
                    errorNumber++;
                }

                callback?.SetNumberOfSent(++sentNumber);
            }

            callback?.SetStatusMessage("メール送信完了");
            callback?.OnSendFinished(totalNumber, errorNumber);
        }

        /// <summary>
        /// 担当者コード未設定のお知らせについて派遣社員を参照して担当者コードを設定する
        /// </summary>
        /// <param name="notices">チェック対象のお知らせリスト</param>
        private void FillManagerCode(List<Notice> notices)
        {
            foreach (var notice in notices)
            {
                if (!string.IsNullOrEmpty(notice.manager_code))
                {
                    continue;
                }

                var managerCode = dispatchWorkers.Where(x => x.dispatch_worker_code == notice.dispatch_worker_code).FirstOrDefault()?.manager_code;

                if (managerCode == null)
                {
                    continue;
                }

                UpdateNoticeManagerCode(notice.dispatch_worker_code, managerCode);
                notice.manager_code = managerCode;
            }
        }

        /// <summary>
        /// メールタイトルの作成
        /// </summary>
        /// <param name="status">お知らせ状況</param>
        /// <returns>メールタイトル</returns>
        private string MakeSubject(int? status)
        {
            return "社会保険連絡メール　" + (status == Constants.StatusOK ? "ＯＫ" : "ＮＧ");
        }

        /// <summary>
        /// メール本文の作成
        /// </summary>
        /// <param name="toName">宛名（○○ 様の部分）</param>
        /// <param name="noticesOnSameMail">お知らせリスト（1件のメールにまとめて送信するお知らせリスト）</param>
        /// <returns>メール本文</returns>
        private string MakeBody(string toName, List<Notice> noticesOnSameMail)
        {
            var body = new StringBuilder();

            foreach (var notice in noticesOnSameMail)
            {
                var dispatchWorkerName = dispatchWorkers.Where(x => x.dispatch_worker_code == notice.dispatch_worker_code).FirstOrDefault()?.name_kana ?? string.Empty;

                body.AppendLine($"{toName}　様");
                body.AppendLine();
                body.AppendLine("・「本日の加入及び喪失書類受取状況については");
                body.AppendLine("下記の通りです。」");
                body.AppendLine();
                body.AppendLine(MakeNoticeBody(notice, dispatchWorkerName));
                body.AppendLine();
            }

            body.AppendLine(signature);

            return body.ToString();
        }

        /// <summary>
        /// メール本文内のお知らせ部分の作成
        /// </summary>
        /// <param name="notice">お知らせ</param>
        /// <param name="dispatchWorkerName">派遣社員名</param>
        /// <returns>お知らせ部分の文字列</returns>
        private string MakeNoticeBody(Notice notice, string dispatchWorkerName)
        {
            if (notice.status == Constants.StatusOK)
            {
                return $"{notice.dispatch_worker_code}　{dispatchWorkerName}　ＯＫ";
            }

            var joinLossString = notice.join_loss_class == Constants.ClassJoin ? "加入" : "喪失";

            var noticeBody = new StringBuilder();
            noticeBody.AppendLine($"{notice.dispatch_worker_code}　{dispatchWorkerName}　ＮＧ");
            noticeBody.AppendLine();
            noticeBody.AppendLine($"{joinLossString}保留　不備書類名称");
            noticeBody.AppendLine(MakeDeferReasonString(notice));

            if (!string.IsNullOrEmpty(notice.remark_1))
            {
                noticeBody.AppendLine();
                noticeBody.AppendLine($"※{notice.remark_1}");
            }

            if (!string.IsNullOrEmpty(notice.remark_2))
            {
                noticeBody.AppendLine();
                noticeBody.AppendLine($"※{notice.remark_2}");
            }

            return noticeBody.ToString();
        }

        /// <summary>
        /// メール本文内の保留理由部分の作成
        /// </summary>
        /// <param name="notice">お知らせ</param>
        /// <returns>保留理由部分の文字列</returns>
        private string MakeDeferReasonString(Notice notice)
        {
            var deferNames = new List<string>();

            deferNames.Add(GetDeferName(notice.defer_reason_1));
            deferNames.Add(GetDeferName(notice.defer_reason_2));
            deferNames.Add(GetDeferName(notice.defer_reason_3));
            deferNames.Add(GetDeferName(notice.defer_reason_4));
            deferNames.Add(GetDeferName(notice.defer_reason_5));

            return string.Join(Environment.NewLine, deferNames.Where(x => !string.IsNullOrEmpty(x)).ToList());
        }

        /// <summary>
        /// 保留理由内容の取得
        /// </summary>
        /// <param name="deferCode">保留理由コード</param>
        /// <returns>表示用の保留理由内容</returns>
        private string GetDeferName(int? deferCode)
        {
            var deferName = deferReasons.Where(x => x.defer_code == deferCode).FirstOrDefault()?.defer_name;
            return deferName != null ? $"・{deferName}" : null;
        }

        /// <summary>
        /// お知らせの担当者コード更新
        /// </summary>
        /// <param name="dispatchWorkerCode">更新対象の派遣社員コード</param>
        /// <param name="managerCode">担当者コード</param>
        private void UpdateNoticeManagerCode(string dispatchWorkerCode, string managerCode)
        {
            var conditions = new Hashtable();
            conditions.Add("dispatch_worker_code", dispatchWorkerCode);
            conditions.Add("manager_code", managerCode);
            conditions.Add("updated_at", DateTime.Now);
            conditions.Add("updated_by", Program.loginInfo.employee_no);

            GropShareSqlMapper.GetInstance().Update("UpdateNoticeManagerCode", conditions);
        }

        /// <summary>
        /// 送信後のお知らせ更新（成功時）
        /// </summary>
        /// <param name="notices">更新対象お知らせリスト</param>
        private void UpdateNoticeOnSuccess(List<Notice> notices)
        {
            var conditions = new Hashtable();
            conditions.Add("dispatch_worker_codes", notices.Select(x => x.dispatch_worker_code).ToArray());
            conditions.Add("mail_send_flag", Constants.FlagOff);
            conditions.Add("mail_send_datetime", DateTime.Now);
            conditions.Add("mail_send_result", "送信成功");
            conditions.Add("updated_at", DateTime.Now);
            conditions.Add("updated_by", Program.loginInfo.employee_no);

            GropShareSqlMapper.GetInstance().Update("UpdateNoticeOnMailSendSuccess", conditions);
        }

        /// <summary>
        /// 送信後のお知らせ更新（失敗時）
        /// </summary>
        /// <param name="notices">更新対象お知らせリスト</param>
        /// <param name="errorMessage">エラーメッセージ</param>
        private void UpdateNoticeOnFailed(List<Notice> notices, string errorMessage)
        {
            var conditions = new Hashtable();
            conditions.Add("dispatch_worker_codes", notices.Select(x => x.dispatch_worker_code).ToArray());
            conditions.Add("mail_send_result", errorMessage.Cut(Constants.MailSendResultMaxLength));
            conditions.Add("updated_at", DateTime.Now);
            conditions.Add("updated_by", Program.loginInfo.employee_no);

            GropShareSqlMapper.GetInstance().Update("UpdateNoticeOnMailSendFailed", conditions);
        }
    }
}