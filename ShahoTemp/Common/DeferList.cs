﻿using GropCommonLib;
using DensanKyuuyo.Bl;
using System;

namespace DensanKyuuyo.Common
{
    public class DeferList
    {
        /// <summary>
        /// 保留リストをEXCEL出力する。
        /// </summary>
        public void DeferListExcelOutput()
        {
            string fileName = "保留リストExcel_" + GNumStringUtil.GetFileNameDatePrefix() + ".xlsx";
            string file = GFileUtil.ShowSaveFileDialog(GFileUtil.GetDesktopPath(), fileName);

            if (file == string.Empty)
            {
                return;
            }

            try
            {
                var bl = new DeferListBl();
                var deferlist = bl.SearchNoticeForExcel();
                bl.ExcelOutput(deferlist, file);
                GMessageUtil.Information("Excelファイルを出力しました。");
            }
            catch (Exception ee)
            {
                GMessageUtil.Halt("お知らせ情報取得処理でエラーが発生しました。" + ee.Message);
            }
        }
    }
}
